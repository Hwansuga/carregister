﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Runtime.InteropServices;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Security.Principal;
using System.Security;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Web;
//using Excel = Microsoft.Office.Interop.Excel;
//using InputManager;

public enum FORMPOS
{
    FROMPOS_RIGHT = 0,
    FROMPOS_LEFT = 1,
    FROMPOS_UP = 2,
    FROMPOS_DOWN = 3,
}

class Util : Singleton<Util>
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Rect
    {
        public int left;
        public int top;
        public int right;
        public int bottom;
    }

    [DllImport("kernel32", CharSet = CharSet.Unicode)]
    public static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
    [DllImport("kernel32", CharSet = CharSet.Unicode)]
    public static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal,
                                                    int size, string filePath);
    [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
    private static extern int GetPrivateProfileSection(string lpAppName, byte[] lpszReturnBuffer, int nSize, string lpFileName);

    [DllImport("user32.dll")]
    public static extern IntPtr GetWindowRect(IntPtr hWnd, out Rect rect);

    [DllImport("user32.dll")]
    public static extern bool PrintWindow(IntPtr hWnd, IntPtr hdcBlt, int nFlags);

    [DllImport("user32.dll")]
    static extern bool SetForegroundWindow(IntPtr hWnd);

    [DllImport("user32.dll")]
    static extern IntPtr GetWindowDC(IntPtr hWnd);
    [DllImport("user32.dll")]
    static extern IntPtr ReleaseDC(IntPtr hWnd, IntPtr hDC);
    [DllImport("gdi32.dll")]
    static extern IntPtr CreateCompatibleDC(IntPtr hDC);

    [DllImport("gdi32.dll")]
    internal static extern IntPtr CreateCompatibleBitmap(IntPtr hDC, int nWidth, int nHeight);

    [DllImport("gdi32.dll")]
    internal static extern IntPtr SelectObject(IntPtr hDC, IntPtr hObject);

    [DllImport("gdi32.dll", SetLastError = true)]
    static extern bool BitBlt(IntPtr hObject, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hObjectSource, int nXSrc, int nYSrc, int dwRop);
    const int SRCCOPY = 0x00CC0020;

    [DllImport("gdi32.dll")]
    internal static extern bool DeleteDC(IntPtr hDC);
    [DllImport("gdi32.dll")]
    internal static extern bool DeleteObject(IntPtr hObject);

    [DllImport("user32.dll", SetLastError = true)]
    static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int Width, int Height, bool Repaint);


    public static void AutoLineStringAdd(ListBox listBox_, string strMsg_ , bool timeStamp_ = true)
    {
        string strMsg = strMsg_;
        if (timeStamp_)
            strMsg = DateTime.Now.ToString("HH:mm:ss | ") + strMsg_;

        Graphics g = listBox_.CreateGraphics();
        Font font = listBox_.Font;
        int nWidth = listBox_.Width;

        string strTmp = "";
        foreach (char item in strMsg)
        {
            int nTempWidth = (int)g.MeasureString(strTmp + item, font).Width;
            if (nTempWidth > nWidth)
            {
                listBox_.Items.Add(strTmp);
                strTmp = "";
                strTmp += item;
            }
            else
            {
                strTmp += item;
            }
        }

        if (string.IsNullOrEmpty(strTmp) == false)
            listBox_.Items.Add(strTmp);

        listBox_.SelectedIndex = listBox_.Items.Count - 1;
    }

    public static bool CheckDupleForm(string FormName)
    {
        bool flag = false;
        foreach (Form form in Application.OpenForms)
        {
            if (form.Name == FormName)
            {
                form.Activate();
                flag = true;
                return flag;
            }
        }
        flag = false;
        return flag;
    }

    public static string GetAlphabet(int value)
    {
        string result = string.Empty;
        while (--value >= 0)
        {
            result = (char)('A' + value % 26) + result;
            value /= 26;
        }
        return result;
    }

    public static void SetFormPos(Form standard_, Form targetForm_, FORMPOS typePos = FORMPOS.FROMPOS_DOWN)
    {
        switch (typePos)
        {
            case FORMPOS.FROMPOS_RIGHT:
                targetForm_.Location = new System.Drawing.Point(standard_.Location.X + standard_.Width, standard_.Location.Y);
                break;
            case FORMPOS.FROMPOS_LEFT:
                targetForm_.Location = new System.Drawing.Point(standard_.Location.X - targetForm_.Width, standard_.Location.Y);
                break;
            case FORMPOS.FROMPOS_UP:
                targetForm_.Location = new System.Drawing.Point(standard_.Location.X, standard_.Location.Y - targetForm_.Height);
                break;
            case FORMPOS.FROMPOS_DOWN:
                targetForm_.Location = new System.Drawing.Point(standard_.Location.X, standard_.Location.Y + standard_.Height);
                break;
        }
    }

    public static void Adb_Send(string arg)
    {
        try
        {
            Process process = new Process();
            process.StartInfo.FileName = string.Concat(Application.StartupPath, "\\adb.exe");
            process.StartInfo.Arguments = arg;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            process.Start();
            process.WaitForExit();
            process.Close();
        }
        catch
        {
        }
    }

    public static string Get_IP()
    {
        string end = "";
        HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create("http://autopromaker.com/myip.php");
        httpWebRequest.Method = "GET";
        httpWebRequest.Timeout = 30000;
        using (HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse())
        {
            using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            {
                end = streamReader.ReadToEnd();
            }
        }
        return end;
    }

    public static void LoadTxtFile(string fileName_, ref List<string> listLineInfo_, string additionPath_ = "")
    {
        string path = Directory.GetCurrentDirectory();
        if (string.IsNullOrEmpty(additionPath_) == false)
            path += "//" + additionPath_;

        if (string.IsNullOrEmpty(fileName_) == false)
            path += "//" + fileName_;

        listLineInfo_.Clear();

        if (File.Exists(path) == false)
            return;

        StreamReader file = new System.IO.StreamReader(path);
        string line;
        while ((line = file.ReadLine()) != null)
        {
            if (string.IsNullOrEmpty(line))
                continue;

            listLineInfo_.Add(line);
        }
        file.Close();
    }
       
    public static Process OpenTxtFile(string fileName_, string additionPath_ = "")
    {
        string path = Directory.GetCurrentDirectory();
        if (string.IsNullOrEmpty(additionPath_) == false)
            path += "//" + additionPath_;

        if (string.IsNullOrEmpty(fileName_) == false)
            path += "//" + fileName_;

        return Process.Start(path);
    }

    public static void SaveFile(string path_, List<string> listData_)
    {
        if (File.Exists(path_))
            File.Delete(path_);

        var fileInfo = new FileInfo(path_);
        fileInfo.Directory.Create();

        File.WriteAllLines(path_, listData_.ToArray());
    }

    public static void SaveTxtFile(string fileName_, List<string> listData_, string additionPath_ = "")
    {
        string path = Directory.GetCurrentDirectory();
        if (string.IsNullOrEmpty(additionPath_) == false)
            path += "\\" + additionPath_;

        if (Directory.Exists(path) == false)
            Directory.CreateDirectory(path);

        if (string.IsNullOrEmpty(fileName_) == false)
            path += "\\" + fileName_;

        if (File.Exists(path))
            File.Delete(path);

        System.IO.File.WriteAllLines(path, listData_.ToArray());

    }
/*
    public static List<T> LoadExcelFile<T>(string fileName_, string additionPath_ = "")
    {
        string path = Directory.GetCurrentDirectory();
        if (string.IsNullOrEmpty(additionPath_) == false)
            path += "//" + additionPath_;

        if (string.IsNullOrEmpty(fileName_) == false)
            path += "//" + fileName_;

        if (File.Exists(path) == false)
            return null;

        Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
        Microsoft.Office.Interop.Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(path);
        Microsoft.Office.Interop.Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
        Microsoft.Office.Interop.Excel.Range xlRange = xlWorksheet.UsedRange;

        List<T> ret = new List<T>();


        int rowCount = xlRange.Rows.Count;
        int colCount = xlRange.Columns.Count;

        for (int i = 2; i <= rowCount; i++)
        {
            T temp = (T)Activator.CreateInstance(typeof(T));
            FieldInfo[] listFields = temp.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance);
            for (int j = 1; j <= colCount; j++)
            {
                if (xlRange.Cells[i, j] == null)
                    continue;
                if (xlRange.Cells[i, j].Value == null)
                    continue;

                string strValue = xlRange.Cells[i, j].Value.ToString();

                listFields[j - 1].SetValue(temp, strValue);
            }
            ret.Add(temp);
        }

        GC.Collect();
        GC.WaitForPendingFinalizers();
        Marshal.ReleaseComObject(xlRange);
        Marshal.ReleaseComObject(xlWorksheet);
        xlWorkbook.Close();
        Marshal.ReleaseComObject(xlWorkbook);
        xlApp.Quit();
        Marshal.ReleaseComObject(xlApp);

        return ret;
    }

    public static void SaveExcelFile(string fileName_, List<object> listData_, string additionPath_ = "")
    {
        if (listData_.Count <= 0)
            return;

        string path = Directory.GetCurrentDirectory();
        if (string.IsNullOrEmpty(additionPath_) == false)
            path += "\\" + additionPath_;

        if (Directory.Exists(path) == false)
            Directory.CreateDirectory(path);

        if (string.IsNullOrEmpty(fileName_) == false)
            path += "\\" + fileName_;

        try
        {
            if (File.Exists(path))
                File.Delete(path);
        }
        catch
        {
            MessageBox.Show(path + " 파일이 사용중에 있습니다.");
            return;
        }

        FieldInfo[] fieldInfos = listData_[0].GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

        int column = fieldInfos.Length ;
        int row = listData_.Count;

        Excel.Application excelApp = null;
        Excel.Workbook wb = null;
        Excel.Worksheet ws = null;
        object misValue = System.Reflection.Missing.Value;
        try
        {
            excelApp = new Excel.Application();

            wb = excelApp.Workbooks.Add(misValue);
            ws = wb.Worksheets.get_Item(1) as Excel.Worksheet;
            for (int i=0; i<row; ++i)
            {
                object infoData = listData_[i];
                for (int k=0; k<column; ++k)
                {
                    string targetData = fieldInfos[k].GetValue(infoData).ToString();
                    ws.Cells[i+1 , k+1] = "'"+targetData;
                }
            }

            wb.SaveAs(path, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);

            wb.Close(true, misValue, misValue);
            excelApp.Quit();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            ReleaseExcelObject(ws);
            ReleaseExcelObject(wb);
            ReleaseExcelObject(excelApp);
        }

    }

    private static void ReleaseExcelObject(object obj)
    {
        try
        {
            if (obj != null)
            {
                Marshal.ReleaseComObject(obj);
                obj = null;
            }
        }
        catch (Exception ex)
        {
            obj = null;
            throw ex;
        }
        finally
        {
            GC.Collect();
        }
    }
*/

    public static T PopItemFromList<T>(List<T> list_)
    {
        if (list_.Count <= 0)
            return default(T);

        T frontItem = list_[0];

        list_.Remove(frontItem);
        return frontItem;
    }

    public static T ToEnum<T>(string value_)
    {
        return (T)Enum.Parse(typeof(T), value_, true);
    }

    public static void SetValue(object targetObj_, string fieldName_, object value_)
    {
        if (value_ == null)
            return;

        FieldInfo field = targetObj_.GetType().GetField(fieldName_, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance);
        if (field != null)
        {
            object newValue = null;
            if (field.FieldType.IsClass)
            {
                if(field.FieldType == typeof(ComboBox))
                {
                    ComboBox fieldData = (ComboBox)field.GetValue(targetObj_);
                    if(fieldData.Items.Contains(value_.ToString()))
                        fieldData.SelectedItem = value_.ToString();
                }
                else if (field.FieldType == typeof(TextBox))
                {
                    TextBox fieldData = (TextBox)field.GetValue(targetObj_);
                    fieldData.Text = value_.ToString();
                }
                else if (field.FieldType == typeof(Label))
                {
                    Label fieldData = (Label)field.GetValue(targetObj_);
                    fieldData.Text = value_.ToString();
                }
                else if (field.FieldType == typeof(RadioButton))
                {
                    RadioButton fieldData = (RadioButton)field.GetValue(targetObj_);
                    fieldData.Checked = bool.Parse(value_.ToString());
                }
                else if (field.FieldType == typeof(CheckBox))
                {
                    CheckBox fieldData = (CheckBox)field.GetValue(targetObj_);
                    fieldData.Checked = bool.Parse(value_.ToString());
                }
                else if (field.FieldType == typeof(NumericUpDown))
                {
                    NumericUpDown fieldData = (NumericUpDown)field.GetValue(targetObj_);
                    fieldData.Value = int.Parse(value_.ToString() , NumberStyles.AllowThousands);
                }
                else
                {
                    field.SetValue(targetObj_, value_.ToString());
                }
            }
            else
            {
                if (field.FieldType.IsEnum)
                {
                    newValue = Enum.Parse(field.FieldType, value_.ToString());
                }
                else
                {
                    newValue = Convert.ChangeType(value_.ToString().Replace(",",""), field.FieldType);
                }

                field.SetValue(targetObj_, newValue);
            }          
        }
    }

    public static int GetIntFromBitArray(BitArray bitArray_)
    {
        if (bitArray_.Length > 32)
            throw new ArgumentException("Argument length shall be at most 32 bits.");

        int[] array = new int[1];
        bitArray_.CopyTo(array, 0);
        return array[0];
    }

    public static void KillProcess(string processName_)
    {
        Process[] processlist = Process.GetProcesses();
        foreach (Process theprocess in processlist)
        {
            //크롬
            if (theprocess.ProcessName.Contains(processName_))
            {
                theprocess.Kill();
            }
        }
    }

    public static List<FileInfo> GetFileList(string DirPath_ , string format_)
    {
        string path = Directory.GetCurrentDirectory();
        if (string.IsNullOrEmpty(DirPath_) == false)
            path += "\\" + DirPath_;

        if (Directory.Exists(path) == false)
            return new List<FileInfo>();

        DirectoryInfo d = new DirectoryInfo(path);

        FileInfo[] files = d.GetFiles("*." + format_);

        return files.ToList();
    }

    public static string ReverseString(string str_)
    {
        char[] arr = str_.ToCharArray();
        Array.Reverse(arr);
        return new string(arr);
    }

    public static string DataFromRestApi(string url_ , string pram_)
    {
        HttpClient client = new HttpClient();
        client.BaseAddress = new Uri(url_);
        client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
        HttpResponseMessage response = client.GetAsync(pram_).Result;

        if (response.IsSuccessStatusCode)
        {
            return response.Content.ReadAsStringAsync().Result;
        }
        else
        {
            return "";
        }
    }

    public static bool CheckDuplication(string key_)
    {
        string mtxName = key_;
        Mutex mtx = new Mutex(true, mtxName);

        // 1초 동안 뮤텍스를 획득하려 대기  
        TimeSpan tsWait = new TimeSpan(0, 0, 1);
        bool success = mtx.WaitOne(tsWait);

        // 실패하면 프로그램 종료  
        if (!success)
        {
            MessageBox.Show("이미 실행중입니다.");
            return true;

        }
        return false;
    }

    public static string GetNumber(string num_)
    {
        string temp = num_.Replace(",","");

        temp = Regex.Replace(temp, @"[^0-9\.]+", string.Empty);

        return temp;
       
    }

    public  static void CopyClass<T>(T copyFrom, T copyTo)
    {
        if (copyFrom == null || copyTo == null)
            return;

        var properties = copyFrom.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance);

        foreach (var p in properties)
        {
            object copyValue = p.GetValue(copyFrom);
            p.SetValue(copyTo, copyValue);
        }
    }

    public static DateTime GetCurDateFromInt()
    {
        var myHttpWebRequest = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
        var response = myHttpWebRequest.GetResponse();
        string todaysDates = response.Headers["date"];
        DateTime ret = DateTime.ParseExact(todaysDates,
                                   "ddd, dd MMM yyyy HH:mm:ss 'GMT'",
                                   CultureInfo.InvariantCulture.DateTimeFormat,
                                   DateTimeStyles.AssumeUniversal);

        return ret;
    }

    public static bool CheckExpireDate(DateTime limitData_)
    {
        DateTime now = GetCurDateFromInt();
        int ret = DateTime.Compare(now, limitData_);
        if (ret > 0)
            return true;
        else
            return false;
    }

    public static bool IsAdministrator()
    {
        WindowsIdentity identity = WindowsIdentity.GetCurrent();

        if (null != identity)
        {
            WindowsPrincipal principal = new WindowsPrincipal(identity);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }

        return false;
    }

    public static string SecureStringToString(SecureString secureString)
    {
        return Marshal.PtrToStringUni(Marshal.SecureStringToGlobalAllocUnicode(secureString));
    }

    public static string Sha512(string plainText)
    {
        var result = string.Empty;

        foreach (var item in new SHA512Managed().ComputeHash(Encoding.UTF8.GetBytes(plainText)))
        {
            result += item.ToString("x2");
        }

        return result;
    }

    public static void SaveValueAtIni(object obj_, string fileName_, string additionPath_ = "" , string fieldKeyWord_ = "")
    {
        string path = Directory.GetCurrentDirectory();
        if (string.IsNullOrEmpty(additionPath_) == false)
            path += "\\" + additionPath_;

        if (Directory.Exists(path) == false)
            Directory.CreateDirectory(path);

        if (string.IsNullOrEmpty(fileName_) == false)
            path += "\\" + fileName_;

        List<FieldInfo> listFields = obj_.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance).ToList();
        if (string.IsNullOrEmpty(fieldKeyWord_) == false)
            listFields = listFields.FindAll(x => x.Name.Contains(fieldKeyWord_));
        foreach (FieldInfo item in listFields)
        {

            if (item.GetValue(obj_) == null)
                continue;

            WritePrivateProfileString("Variable", item.Name, item.GetValue(obj_).ToString(), path);
        }
    }

    public static void LoadValueInIni(object obj_, string fileName_, string additionPath_ = "" , string fieldKeyWord_ = "")
    {
        string path = Directory.GetCurrentDirectory();
        if (string.IsNullOrEmpty(additionPath_) == false)
            path += "\\" + additionPath_;

        if (string.IsNullOrEmpty(fileName_) == false)
            path += "\\" + fileName_;

        if (File.Exists(path) == false)
            return;

        byte[] buffer = new byte[2048];
        GetPrivateProfileSection("Variable", buffer, 2048, path);
        string[] tmp = Encoding.Unicode.GetString(buffer).Trim('\0').Split('\0');

        List<FieldInfo> listFields = obj_.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance).ToList();
        if (string.IsNullOrEmpty(fieldKeyWord_) == false)
            listFields = listFields.FindAll(x => x.Name.Contains(fieldKeyWord_));
        foreach (string item in tmp)
        {
            string[] keyValue = item.Split('=');
            SetValue(obj_ , keyValue[0] , keyValue[1]);
        }
    }

    public static List<List<T>> SplitList<T>(List<T> me, int size = 50)
    {
        var list = new List<List<T>>();
        for (int i = 0; i < me.Count; i += size)
            list.Add(me.GetRange(i, Math.Min(size, me.Count - i)));
        return list;
    }

    public static string GetSubstringByString(string a, string b, string c)
    {
        if (c.Contains(a) == false || c.Contains(b) == false)
            return c;
        return c.Substring((c.IndexOf(a) + a.Length), (c.IndexOf(b) - c.IndexOf(a) - a.Length));
    }

    public static void Typingid(string str)
    {
        string path = Application.StartupPath;
        string program = "" + path + "\\typinga.exe";
        try
        {
            Clipboard.SetText(str);
        }
        catch (Exception ex)
        {

        }

        //ProcessStartInfo startinfo = new ProcessStartInfo("cmd.exe", msg);
        ProcessStartInfo startinfo = new ProcessStartInfo(program);
        Process process = new Process();
        startinfo.WindowStyle = ProcessWindowStyle.Hidden;
        startinfo.CreateNoWindow = true;

        process.StartInfo = startinfo;
        process.Start();
        process.WaitForExit();
        Thread.Sleep(100);
        //process.Close();
        //process.Kill();
    }

    public static void Typingpw(string str)
    {
        string path = Application.StartupPath;
        string program = "" + path + "\\typingb.exe";
        Clipboard.SetText(str);
        //ProcessStartInfo startinfo = new ProcessStartInfo("cmd.exe", msg);
        ProcessStartInfo startinfo = new ProcessStartInfo(program);
        Process process = new Process();
        startinfo.WindowStyle = ProcessWindowStyle.Hidden;
        startinfo.CreateNoWindow = true;

        process.StartInfo = startinfo;
        process.Start();
        process.WaitForExit();
        Thread.Sleep(100);
        //process.Close();
        //process.Kill();
    }

    public static List<T> GetFieldListByName<T>(object obj_, string name_)
    {
        List<FieldInfo> listField = obj_.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).ToList();
        List<FieldInfo> listT = listField.FindAll(x => x.FieldType == typeof(T) && x.Name.Contains(name_));

        return listT.ConvertAll(x => (T)x.GetValue(obj_));
    }

    static bool IsWindows10()
    {
        var reg = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion");

        string productName = (string)reg.GetValue("ProductName");

        return productName.StartsWith("Windows 10");
    }

    public static void SortByNameNymber(List<FieldInfo> listInfo_)
    {
        listInfo_.Sort((x, y) =>
        int.Parse(Regex.Replace(x.Name, @"\D", ""))
        .CompareTo(int.Parse(Regex.Replace(y.Name, @"\D", "")))
        );
    }
    public static void SortByNameNymber(List<string> listInfo_)
    {
        listInfo_.Sort((x, y) =>
        int.Parse(Regex.Replace(x, @"\D", ""))
        .CompareTo(int.Parse(Regex.Replace(y, @"\D", "")))
        );
    }


    public static string GetValueBetweenBlackets(string data_)
    {
        return Regex.Match(data_, @"\(([^)]*)\)").Groups[1].Value;
    }

    public static string GetValueBetweenChar(string data_, char seperator_)
    {
        Regex r = new Regex($@"{seperator_}(.+?){seperator_}");
        MatchCollection mc = r.Matches(data_);
        return mc[0].Groups[1].Value;
    }

    public static string ToQueryString(NameValueCollection nvc)
    {
        var array = (
            from key in nvc.AllKeys
            from value in nvc.GetValues(key)
            select string.Format(
            "{0}={1}",
            HttpUtility.UrlEncode(key),
            HttpUtility.UrlEncode(value))
            ).ToArray();
        return "?" + string.Join("&", array);
    }
}

