﻿using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarRegister
{
    public partial class Form_CarRegister : Form
    {
        ConsoleTextBoxWriter logger;
        System.Windows.Forms.Timer reaminTimer = null;

        List<CheckBox> listOption = new List<CheckBox>();

        public PageCarmodoo car = new PageCarmodoo();
        PageCha pageCha = new PageCha();

        public Form_CarRegister()
        {
            InitializeComponent();
            logger = new ConsoleTextBoxWriter(richTextBox_Log);
        }

        public void PrintLog(string msg_)
        {
            logger.WriteLine(msg_);
        }

        public void PrintLog(string msg_, Color color_)
        {
            logger.WriteLineWithColor(msg_, color_);
        }

        private void Form_CarRegister_Load(object sender, EventArgs e)
        {
#if DEBUG         
            Util.KillProcess("chromedriver");
#else
            button_Test.Visible = false;
            Util.KillProcess("chrome");
#endif
            //WebClient wc = new WebClient();
            //for (int i=0; i<6; ++i)
            //{
            //    string name = $@"Img\43나\{i}.png";
            //    if (File.Exists(name))
            //        File.Delete(name);
            //    var fileInfo = new FileInfo(name);
            //    fileInfo.Directory.Create();
            //    wc.DownloadFile("https://search.pstatic.net/common/?src=http%3A%2F%2Fblogfiles.naver.net%2FMjAyMTAyMTdfMTc0%2FMDAxNjEzNTYwMDQ5Njg5.ayJq6bbi-tm0fEeMIhjph5r7OaGZ0Fhqz9K2tjyZ9pQg.aRSoCt6ULr39IhjuCa9kINZU_p0SD07CGgql3aV2otMg.JPEG.vamtol3%2Fsunny.jpg&type=sc960_832", name);
            //}


            EventUtil.ConnectCustomEvent(this);
#if VER2
            Global.uiController = this;
#endif

            Global.LoadDB();
            Global.LoadDesc();
            car.GetGubun();

        }

        void CreateOption()
        {
            foreach (var item in panel_Option.Controls.OfType<CheckBox>().ToList())
            {
                panel_Option.Controls.Remove(item);
            }

            listOption.Clear();
            foreach (var item in Global.dicAcNo)
            {
                var checkBox = new CheckBox();              
                panel_Option.Controls.Add(checkBox);

                checkBox.Text = item.Key;
                checkBox.Dock = DockStyle.Top;
                checkBox.Name = item.Key;
                checkBox.BringToFront();

                listOption.Add(checkBox);
            }
        }

        public void UpdateTopItemLabel()
        {

        }

        void Worker()
        {
            pageCha.CollectCookie();
            var checkedAcNo = listOption.FindAll(x => x.Checked).Select(x => Global.dicAcNo[x.Text]).ToArray();

            var page = 1;
            var db = Global.GetDB("25.txt");
            while (true)
            {
                if (Global.stop)
                    break;

                var listItem = car.GetListItemByHttp(100 , checkedAcNo , page);
                if (listItem.Count <= 0)
                {
                    PrintLog("모든 페이지 검색완료");
                    break;
                }
                    

                PrintLog($"================================================", Color.RoyalBlue);
                PrintLog($"Page : {page} - 가져온 정보 개수 : {listItem.Count}" , Color.RoyalBlue);
                PrintLog($"================================================", Color.RoyalBlue);

                listItem = listItem.FindAll(x => db.Contains(x.num) == false);
                PrintLog($"필터 후 정보 개수 : {listItem.Count}", Color.RoyalBlue);
                PrintLog($"================================================", Color.RoyalBlue);


                foreach (var item in listItem)
                {
                    if (Global.stop)
                        break;                        
                    try
                    {
                        var checkNo = pageCha.CheckNo(item.num);
                        if (checkNo["errCode"].ToString() == "8")
                        {
                            var del = pageCha.DelStep(checkNo);
                            checkNo = pageCha.CheckNo(item.num);
                        }

                        if (checkNo["errCode"].ToString() != "0")
                        {
                            PrintLog($"{item.num} 이미 등록된 번호");
                            continue;
                        }


                        PrintLog($"{item.num} 등록 시도", Color.RosyBrown);
                        var carInfo = car.GetCarInfo(item);
                        if (carInfo.price == "9999" || carInfo.price == "99999")
                        {
                            PrintLog($"{item.num} 이미 팔린 매물 ({carInfo.price}) Pass");
                            continue;
                        }

                        if (pageCha.RegiCar(checkNo, carInfo))
                        {                           
                            var seq = pageCha.ProcessPhotoUpload(carInfo);
                            PrintLog($"{item.num} 등록 성공!", Color.Blue);
                            var retWait = pageCha.RegWaitBySeq(seq);
                            if (retWait["errCode"].ToString() == "0")
                            {
                                PrintLog($"::{carInfo.num} 대기 이동 성공::", Color.Blue);
                            }
                            else
                            {
                                PrintLog($"::{carInfo.num} 대기 이동 실패::");
                            }
                        }
                        else
                        {
                            PrintLog($"{item.num} 등록 실패!", Color.Red);
                        }
                            
                    }
                    catch(Exception ex)
                    {
                        if (ex.Message.Contains("401"))
                        {
                            var ret = MessageBox.Show("차차차 페이지에 재 로그인 해주세요!");
                            if (ret == DialogResult.OK)
                            {
                                pageCha.CollectCookie();
                                Thread.Sleep(1000);
                            }
                        }
                        else
                        {
#if DEBUG
                            PrintLog($"{item.num} 등록중 에러발생 : {ex.Message}!", Color.Red);
#else
                            PrintLog($"{item.num} 등록중 에러발생", Color.Red);
#endif
                        }


                    }
                }

                db.AddRange(listItem.Select(x => x.num).ToList());
                Global.UpdateDB("25.txt");

                PrintLog($"임시저장 삭제 작업");
                try
                {
                    var listTemp = pageCha.GetTempData();
                    foreach (var item in listTemp["list"] as JArray)
                    {
                        var retDel = pageCha.DelTempData(item["carSeq"].ToString());
                        if (retDel["errCode"].ToString() == "0")
                            PrintLog($"{item["carNo"].ToString()} 삭제 완료");
                        else
                            PrintLog($"{item["carNo"].ToString()} 삭제 실패");
                    }
                }
                catch(Exception ex)
                {
                    //PrintLog($"삭제중 에러 발생");
                }
                

                ++page;
            }


            this.Invoke(new MethodInvoker(() =>
            {
                button_Start.Enabled = true;
                button_Stop.Enabled = false;
                button_Reset.Enabled = true;

                panel_Option.Enabled = true;
                tableLayoutPanel_StartTime.Enabled = true;
            }));

            PrintLog("정지 완료", Color.RosyBrown);
        }

        
        void SetReaminTimer(long totalSec_)
        {       
            this.label_RemainSec.Text = $"{totalSec_}초";

            reaminTimer = new System.Windows.Forms.Timer();
            reaminTimer.Interval = 1000;
            reaminTimer.Tick += new EventHandler((object sender_, EventArgs e_) =>
            {
                if (totalSec_ <= 0)
                {

                    reaminTimer.Stop();
                    var woker = new Thread(this.Worker);
                    woker.Start();                    

                    return;
                }
                totalSec_--;
                this.label_RemainSec.Invoke(new MethodInvoker(delegate ()
                {
                    this.label_RemainSec.Text = $"{totalSec_}초";
                }));


            });
            reaminTimer.Start();
        }

        private void button_Ready_Click(object sender, EventArgs e)
        {
            this.button_Start.Enabled = true;
            this.button_Ready.Enabled = false;

            Global.comboBox_Kind = "수원";
            car.QuiteDriver();
            car.Login();

            CreateOption();
        }

        private void button_ReadyRegister_Click(object sender, EventArgs e)
        {
            this.button_ReadyRegister.Enabled = false;
            pageCha.Login();
            PrintLog("페이지 로그인을 해주세요" , Color.Blue);
        }

        private void button_Start_Click(object sender, EventArgs e)
        {
            Global.stop = false;

            PrintLog("시작", Color.RosyBrown);

            button_Start.Enabled = false;
            button_Stop.Enabled = true;
            button_Reset.Enabled = false;

            panel_Option.Enabled = false;
            tableLayoutPanel_StartTime.Enabled = false;

            this.SetReaminTimer((long)(dateTimePicker_Open.Value - DateTime.Now).TotalSeconds);

            pageCha.driver.ExecuteScript("window.focus();");
            Thread.Sleep(1000);
        }

        private void button_Stop_Click(object sender, EventArgs e)
        {
            Global.stop = true;

            reaminTimer.Dispose();
            this.SetReaminTimer(0);

            PrintLog("정지중...", Color.RosyBrown);

            button_Stop.Enabled = false;           
        }

        private void button_Reset_Click(object sender, EventArgs e)
        {
            //pageCha.SetPicture(@"Img\08버1105", 20);
            Global.DeleteDB();
            PrintLog("기록 데이터 삭제");

            //var btnImage = pageCha.driver.FindElementById("frmImageUpload").FindElement(By.Id("file1"));
            //pageCha.driver.ExecuteScript("uploadPhoto.onAttachMultiFile();");

            //pageCha.driver.FindElementById("btnPhotoUploadAll").Click();
            //Thread.Sleep(1000);
          
            ////pageCha.driver.ExecuteScript("delete HTMLInputElement.prototype.click");

            ////Actions action = new Actions(pageCha.driver);
            ////action.SendKeys(OpenQA.Selenium.Keys.Escape).Perform();

            //Thread.Sleep(2000);
        }

        private void button_Test_Click(object sender, EventArgs e)
        {
            pageCha.SetPicture(@"Img\72나7616", 20);
        }
    }
}
