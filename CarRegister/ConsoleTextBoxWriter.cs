﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

public class ConsoleTextBoxWriter : TextWriter
{
    private RichTextBox textBox;
    public bool timeStamp = true;

    public ConsoleTextBoxWriter(RichTextBox textBox)
    {
        Console.SetOut(this);
        this.textBox = textBox;
    }
    public override Encoding Encoding { get { return Encoding.UTF8; } }

    public override void Write(string value)
    {
        WriteImp(value, Color.Black);
    }

    public override void WriteLine(string value)
    {
        WriteImp(value + Environment.NewLine, Color.Black);
    }

    public void WriteLineWithColor(string value, Color color)
    {
        WriteImp(value + Environment.NewLine, color);
    }

    public void Clear()
    {
        if (this.textBox.InvokeRequired)
            this.textBox.Invoke(new MethodInvoker(delegate ()
            {
                textBox.Text = string.Empty;
            }));
        else
        {
            textBox.Text = string.Empty;
        }
    }

    private void WriteImp(string value, Color color)
    {
        var msg = value;
        if (timeStamp)
            msg = $"{DateTime.Now.ToString("yyyy_MM_dd-HH:mm:ss")} || {value}";
        if (this.textBox.InvokeRequired)
            this.textBox.Invoke(new MethodInvoker(delegate ()
            {
                textBox.SelectionColor = color;
                textBox.AppendText(msg);
                textBox.SelectionColor = textBox.ForeColor;
            }));
        else
        {
            textBox.SelectionColor = color;
            textBox.AppendText(msg);
            textBox.SelectionColor = textBox.ForeColor;
        }

    }
}

