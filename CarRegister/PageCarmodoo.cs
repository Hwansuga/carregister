using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Xml;

public class CarItem
{
    public string num { get; set; } = "";
    public string key { get; set; } = "";
}

public class CarRegiInfo : CarItem
{
    public string price { get; set; } = "";
    public List<string> listImg { get; set; } = new List<string>();
    public string c_mileage { get; set; } = "";
    public string c_displacement { get; set; } = "";
    public string c_inNumber { get; set; } = "";
    public string c_gearbox { get; set; } = "";
    public string c_colorName { get; set; } = "";
    public string c_fuel { get; set; } = "";
    public string c_checkNum { get; set; } = "";
    public string c_checkUrl { get; set; } = "";
}

public class PageCarmodoo : DriverController
{
    string categoryUrl = "https://dealer.carmodoo.com/common/ajax/AutoDBCode.html?mode=getComplexInit&ctl=car&a_area=%B0%E6%B1%E2&a_gugun=25";

    string loginUrl = "https://dealer.carmodoo.com/member/login.html";
    string searchUrl = "https://dealer.carmodoo.com/car/_inc_carListPhoto.html?searchChecker=1&mode=&pageSize=5&no=&m_id=&lastAction=&cho=&maker=&model=&series=&saveFlag=&ord=&ordKey=c_regDate&a_area=%EA%B2%BD%EA%B8%B0&a_gugun={0}&c_acNo=&c_cho=&c_bmNo=&c_boInitNo=&c_boNo=&c_bsNo=&c_bdNo=&c_exCate1=&c_exCate3=&c_year1=&c_month1=&c_year2=&c_month2=&c_mileage1=&c_mileage2=&c_exCate2=&c_price1=&c_price2=&c_carNum=&c_dealerName=&c_sangsaName=&c_dealerHp=&carName=&ordKey=c_regDate&a_area=%B0%E6%B1%E2&a_gugun={0}&firstFlag=0&pageSize=3&_=";
    //string refUrl = "https://dealer.carmodoo.com/car/carListPhoto.html?searchChecker=1&mode=&pageSize=20&no=&m_id=&lastAction=&cho=&maker=&model=&series=&saveFlag=&ord=&ordKey=c_regDate&a_area=%EA%B2%BD%EA%B8%B0&a_gugun={0}&c_acNo=&c_cho=&c_bmNo=&c_boInitNo=&c_boNo=&c_bsNo=&c_bdNo=&c_exCate1=&c_exCate3=&c_year1=&c_month1=&c_year2=&c_month2=&c_mileage1=&c_mileage2=&c_exCate2=&c_price1=&c_price2=&c_carNum=&c_dealerName=&c_sangsaName=&c_dealerHp=&carName=&ordKey=c_regDate&a_area=%B0%E6%B1%E2&a_gugun={0}&pageSize=10";


   
    WebClient wc = new WebClient();

    string searCarItemUrl = "https://dealer.carmodoo.com/car/_inc_carListPhoto.html?searchChecker=1&mode=&pageSize={2}&no=&m_id=&lastAction=&cho=&maker=&model=&series=&saveFlag=&ord=&ordKey=c_regDate&a_area=%EA%B2%BD%EA%B8%B0&a_gugun={0}&c_acNo={1}&c_cho=&c_bmNo=&c_boInitNo=&c_boNo=&c_bsNo=&c_bdNo=&c_exCate1=&c_exCate3=&c_year1=&c_month1=&c_year2=&c_month2=&c_mileage1=&c_mileage2=&c_exCate2=&c_price1=&c_price2=&c_carNum=&c_dealerName=&c_sangsaName=&c_dealerHp=&carName=&ordKey=c_regDate&a_area=%B0%E6%B1%E2&a_gugun={0}&firstFlag=0&pageSize={2}&_=";
    string carItemUrl = "http://dealer.carmodoo.com/car/dealerCarView.html?key";

    List<string> cachingData = new List<string>();

    public void Login()
    {
#if VER2
        var disableImage = false;
#else
        var disableImage = false;
#endif

#if DEBUG
        CreatDriver(showBrowser_: true , disableImg_ : disableImage);
#else
        CreatDriver(showBrowser_: false , disableImg_ : disableImage);
#endif

        driver.Navigate().GoToUrl(loginUrl);
        Thread.Sleep(1000);

        driver.FindElementById("login_id").SendKeys("01092109420");
        Thread.Sleep(1000);
        driver.FindElementById("login_pass").SendKeys("0000");
        Thread.Sleep(1000);      

        driver.FindElementByClassName("btn_login").Click();
        Thread.Sleep(1000);


        string fixRefurl = Global.GetUrl();
        driver.Navigate().GoToUrl(Global.GetUrl());
        Thread.Sleep(1000);
        //this.ClickAll();



        wc.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36");
        //wc.Headers.Add("Content-Type", "application/json, charset=UTF-8");
        wc.Headers.Add("Accept", " */*");
        wc.Headers.Add("Host", "dealer.carmodoo.com");
        wc.Headers.Add("Sec-Fetch-Dest", "empty");
        wc.Headers.Add("Sec-Fetch-Mode", "cors");
        wc.Headers.Add("Sec-Fetch-Site", "same-origin");
        wc.Headers.Add("Accept-Encoding", "gzip, deflate, br");
        wc.Headers.Add("Accept-Language", "ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7");

        wc.Headers.Add("Referer", fixRefurl);
        wc.Headers.Add("X-Requested-With", "XMLHttpRequest");
        CollectCookie();
        GetCategory();
    }

    public void GetCategory()
    {
        Global.dicAcNo.Clear();

        var res = wc.OpenRead(categoryUrl);

        XmlReader reader = XmlReader.Create(res);
        reader.MoveToContent();

        while (reader.ReadToFollowing("item"))
        {
            reader.ReadToFollowing("key");
            var key = reader.ReadElementContentAsString("key", reader.NamespaceURI);

            reader.ReadToFollowing("name");
            string name = reader.ReadElementContentAsString("name", reader.NamespaceURI);

            reader.ReadToFollowing("field");
            string field = reader.ReadElementContentAsString("field", reader.NamespaceURI);

            if (field == "c_acNo")
                Global.dicAcNo.Add(name , key);
        }
    }

    public void GetGubun()
    {
        Global.dicGubun.Clear();
        var res = wc.OpenRead(categoryUrl);

        XmlReader reader = XmlReader.Create(res);
        reader.MoveToContent();

        while (reader.ReadToFollowing("item"))
        {
            reader.ReadToFollowing("key");
            var key = reader.ReadElementContentAsString("key", reader.NamespaceURI);

            reader.ReadToFollowing("name");
            string name = reader.ReadElementContentAsString("name", reader.NamespaceURI);

            reader.ReadToFollowing("field");
            string field = reader.ReadElementContentAsString("field", reader.NamespaceURI);


            if (field == "a_gugun")
                Global.dicGubun.Add(name, key);
        }

    }

    public void CollectCookie(WebClient wc_ = null)
    {
        if (driver == null)
            return;

        var myCookies = driver.Manage().Cookies.AllCookies;
        List<string> listCookies = new List<string>();
        foreach (OpenQA.Selenium.Cookie itemCookie in myCookies)
        {
            listCookies.Add($"{itemCookie.Name}={itemCookie.Value}");
        }

        if (wc_ != null)
            wc_.Headers.Add(HttpRequestHeader.Cookie, string.Join(";", listCookies));
        else
            wc.Headers.Add(HttpRequestHeader.Cookie, string.Join(";", listCookies));

        Thread.Sleep(100);
        //QuiteDriver();
    }

    void ClickAll()
    {
        var elem = driver.FindElementByXPath("//*[@id='c_acNoWrap']/div");
        var scrollbar = driver.FindElementByXPath("//*[@id='c_acNoWrap']/div/div[1]");
        MouseOverAction(elem);
        Thread.Sleep(100);
        Scroll_PageUp(scrollbar, 30);
        Thread.Sleep(1000);
        driver.FindElementById("c_acNoOption_all").Click();
        Thread.Sleep(1000);
    }

    void ClickSearch()
    {
        try
        {
            driver.FindElementByClassName("btn_search_blue").Click();
            Thread.Sleep(1000);
        }
        catch
        {
            return;
        }          
    }

    long LongRandom(long min, long max, Random rand)
    {
        byte[] buf = new byte[8];
        rand.NextBytes(buf);
        long longRand = BitConverter.ToInt64(buf, 0);
        return (Math.Abs(longRand % (max - min)) + min);
    }

    public List<string> GetItems()
    {
        try
        {
            DateTime startDt = new DateTime(1970, 1, 1);
            TimeSpan timeSpan = DateTime.UtcNow - startDt;
            long millis = (long)timeSpan.TotalMilliseconds;

            var r = millis;
            string page = wc.DownloadString(searchUrl + r);
            var rgx = new Regex(@"[0-9]{2}[가-힣]{1}[0-9]{4}");

            List<string> listItem = new List<string>();
            foreach (Match match in rgx.Matches(page))
                listItem.Add(match.Value);
            //this.ClickSearch();

            return listItem;
        }
        catch
        {
            return new List<string>() { "" };
        }
    }

    public string GetTopItem()
    {
        try
        {
            this.ClickSearch();
            //html/body/div[8]/div[2]/div[2]/table
            //html/body/div[8]/div[2]/div[2]/table/tbody/tr[2]/td[2]/strong
            return driver.FindElementByClassName("t_carList").FindElement(By.XPath("./tbody/tr[2]/td[2]/strong")).Text;
        }
        catch
        {
#if DEBUG
            Global.uiController.PrintLog("상단 아이템 정보 가져오는데 실패");
#endif
            try
            {
                driver.Navigate().Refresh();
                Thread.Sleep(3000);
                return "";
            }
            catch
            {
                Global.uiController.PrintLog("카모두 페이지 세션 이슈. 페이지 다시 생성");
                Thread.Sleep(3000);
                driver.Quit();
                this.Login();

                Thread.Sleep(2000);
                Global.uiController.UpdateTopItemLabel();
                Global.uiController.PrintLog("페이지 다시 생성 완료");
                return "";
            }           
        }     
    }

    public List<string> GetTartgetItem()
    {
        //         var items = GetItems();
        // 
        //         int indexTop = items.FindIndex(x => x == Global.topItem);
        //         if (indexTop == -1)
        //         {
        //             return items;
        //         }
        //         else
        //         {
        //             return items.GetRange(indexTop, items.Count - indexTop - 1);
        //         }


        try
        {
            List<string> listItem = new List<string>();
            var rootList = driver.FindElementByClassName("t_carList");
            var listCrt = rootList.FindElements(By.XPath("./tbody/tr"));
            foreach (var item in listCrt)
            {
                if (IsContainAsHtml(item, "id=") == false)
                    continue;

                var id = item.FindElement(By.XPath("./td[2]/strong")).Text;

                if (Global.topItem == id)
                    break;

                listItem.Add(id);
            }

            return listItem;
        }
        catch
        {
            return new List<string>();
        }    
    }

    public List<string> GetListItemByHttp()
    {
        string url = searchUrl + (new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds());
        url = string.Format(url, Global.dicCodeName[Global.comboBox_Kind]);

        WebClient tempWc = new WebClient();
        tempWc.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36");
        //wc.Headers.Add("Content-Type", "application/json, charset=UTF-8");
        tempWc.Headers.Add("Accept", " */*");
        tempWc.Headers.Add("Host", "dealer.carmodoo.com");
        tempWc.Headers.Add("Sec-Fetch-Dest", "empty");
        tempWc.Headers.Add("Sec-Fetch-Mode", "cors");
        tempWc.Headers.Add("Sec-Fetch-Site", "same-origin");
        tempWc.Headers.Add("Accept-Encoding", "gzip, deflate, br");
        tempWc.Headers.Add("Accept-Language", "ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7");

        string fixRefurl = Global.GetUrl();
        tempWc.Headers.Add("Referer", fixRefurl);
        tempWc.Headers.Add("X-Requested-With", "XMLHttpRequest");
        CollectCookie(tempWc);

        var ret = tempWc.DownloadString(url);
        tempWc.Dispose();

        HtmlDocument html = new HtmlDocument();
        html.LoadHtml(ret);

        List<string> listItem = new List<string>();
        var nodes = html.DocumentNode.SelectNodes("/table[1]/tr");
        nodes.RemoveAt(0);
        foreach(var item in nodes)
        {
            var num = item.SelectSingleNode("./td[2]/strong").InnerText;
            listItem.Add(num);
        }

        var pureNew = listItem.Except(cachingData).ToList();

        cachingData.AddRange(pureNew);

        return pureNew;
    }

    public List<CarItem> GetListItemByHttp(int pageSize_ , string[] arrAcNo_ , int page_ , int gubun_ = 25)
    {
        string queryString = "";
        queryString += "?searchChecker=1";
        queryString += "&mode=";
        queryString += $"&pageSize={pageSize_}";
        queryString += $"&no=";
        queryString += $"&m_id=";
        queryString += $"&lastAction=";
        queryString += $"&cho=";
        queryString += $"&maker=";
        queryString += $"&model=";
        queryString += $"&series=";
        queryString += $"&saveFlag=";
        queryString += $"&ord=";
        queryString += $"&ordKey=c_regDate";
        queryString += $"&a_area=경기";
        queryString += $"&a_gugun={gubun_}";
        queryString += $"&c_acNo={string.Join(";", arrAcNo_)}";
        queryString += $"&c_cho=";
        queryString += $"&c_bmNo=";
        queryString += $"&c_boInitNo=";
        queryString += $"&c_boNo=";
        queryString += $"&c_bsNo=";
        queryString += $"&c_bdNo=";
        queryString += $"&c_exCate1=";
        queryString += $"&c_exCate3=";
        queryString += $"&c_year1=";
        queryString += $"&c_month1=";
        queryString += $"&c_year2=";
        queryString += $"&c_month2=";
        queryString += $"&c_mileage1=";
        queryString += $"&c_mileage2=";
        queryString += $"&c_exCate2=";
        queryString += $"&c_price1=";
        queryString += $"&c_price2=";
        queryString += $"&c_carNum=";
        queryString += $"&c_dealerName=";
        queryString += $"&c_sangsaName=";
        queryString += $"&c_dealerHp=";
        queryString += $"&carName=";
        queryString += $"&ordKey=c_regDate";
        queryString += $"&a_area=%B0%E6%B1%E2";
        queryString += $"&a_gugun={gubun_}";
        queryString += $"&page={page_}";
        queryString += $"&pageSize={pageSize_}";
        var milSec = (new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds());
        queryString += $"&_={milSec}";

        string url = "https://dealer.carmodoo.com/car/_inc_carListPhoto.html"+ queryString;
        WebClient tempWc = new WebClient();

        string fixRefurl = "https://dealer.carmodoo.com/car/carListPhoto.html?searchChecker=1&mode=&pageSize=20&no=&m_id=&lastAction=&cho=&maker=&model=&series=&saveFlag=&ord=&ordKey=c_regDate&a_area=%EA%B2%BD%EA%B8%B0&a_gugun=25&c_acNo=&c_cho=&c_bmNo=&c_boInitNo=&c_boNo=&c_bsNo=&c_bdNo=&c_exCate1=&c_exCate3=&c_year1=&c_month1=&c_year2=&c_month2=&c_mileage1=&c_mileage2=&c_exCate2=&c_price1=&c_price2=&c_carNum=&c_dealerName=&c_sangsaName=&c_dealerHp=&carName=&ordKey=c_regDate&a_area=%B0%E6%B1%E2&a_gugun=25&pageSize=10";
        tempWc.Headers.Add("Referer", fixRefurl);

        CollectCookie(tempWc);

        var ret = tempWc.DownloadString(url);
        tempWc.Dispose();

        HtmlDocument html = new HtmlDocument();
        html.LoadHtml(ret);

        List<CarItem> listItem = new List<CarItem>();
        var nodes = html.DocumentNode.SelectNodes("/table[1]/tr");
        nodes.RemoveAt(0);
        foreach (var item in nodes)
        {
            var carItem = new CarItem();

            var num = item.SelectSingleNode("./td[2]/strong").InnerText.Trim();            
            carItem.num = num;

            var key = item.SelectSingleNode("./td[3]").GetAttributeValue("onClick", "");
            if (string.IsNullOrEmpty(key) == false)
            {
                var attKey = Util.GetValueBetweenBlackets(key);
                carItem.key = attKey;
                listItem.Add(carItem);
            }            
        }

        return listItem;
    }

    public CarRegiInfo GetCarInfo(CarItem item_)
    {
        string queryString = "";
        queryString += "?mode=view";
        queryString += $"&key={item_.key}";
        var milSec = (new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds());
        queryString += $"&_={milSec}";

        string url = "http://dealer.carmodoo.com/common/ajax/AutoDB.html" + queryString;
        WebClient tempWc = new WebClient();

        string fixRefurl = $"http://dealer.carmodoo.com/car/dealerCarView.html?key={item_.key}&tabStart=1";
        tempWc.Headers.Add("Referer", fixRefurl);

        CollectCookie(tempWc);

        var ret = tempWc.DownloadString(url);
        tempWc.Dispose();

        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(ret);

        var carInfo = new CarRegiInfo();
        carInfo.num = item_.num;
        carInfo.key = item_.key;

        var nodePrice = xmlDoc.GetElementsByTagName("priceStr");
        carInfo.price = nodePrice[0].InnerText.Replace(",", "");

        var c_mileage = xmlDoc.GetElementsByTagName("c_mileage");
        carInfo.c_mileage = c_mileage[0].InnerText;
        var c_displacement = xmlDoc.GetElementsByTagName("c_displacement");
        carInfo.c_displacement = c_displacement[0].InnerText;
        var c_inNumber = xmlDoc.GetElementsByTagName("c_inNumber");
        carInfo.c_inNumber = c_inNumber[0].InnerText;

        var c_fuel = xmlDoc.GetElementsByTagName("c_fuel");
        carInfo.c_fuel = c_fuel[0].InnerText;
        var c_colorName = xmlDoc.GetElementsByTagName("c_colorName");
        carInfo.c_colorName = c_colorName[0].InnerText;
        var c_gearbox = xmlDoc.GetElementsByTagName("c_gearbox");
        carInfo.c_gearbox = c_gearbox[0].InnerText;

        var c_checkNum = xmlDoc.GetElementsByTagName("c_checkNum");
        carInfo.c_checkNum = c_checkNum[0].InnerText;
        var c_checkUrl = xmlDoc.GetElementsByTagName("c_checkUrl");
        carInfo.c_checkUrl = c_checkUrl[0].InnerText;

        var nodePhoto = xmlDoc.GetElementsByTagName("strPhotos");
        carInfo.listImg = nodePhoto[0].InnerText.Split(new string[] { "##" } , StringSplitOptions.None).ToList();

        carInfo.listImg = carInfo.listImg.ConvertAll(x => x = $"http://dealer.carmodoo.com{x}");

        return carInfo;

    }
}

