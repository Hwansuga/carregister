﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarRegister
{
    static class Program
    {
        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main()
        {

            string mutexKey = "{199FCD5E-F7B0-4967-9DE0-FA60DCE2CC16}";

            //check duplication
            if (Util.CheckDuplication(mutexKey))
                return;

//             if (Util.CheckExpireDate(new DateTime(2020, 9, 19)))
//             {
//                 MessageBox.Show("기간이 만료되었습니다.");
//                 return;
//             }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
#if VER2
            Application.Run(new Form_CarRegister());
#else
            Application.Run(new Form1());
#endif
        }
    }
}
