﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


class SingletoneLazy<T> where T : class
{
    #region Field
    private static T instance;
    #endregion

    private static readonly Lazy<T> Lazy = new Lazy<T>(() =>
    {
        var ctors = typeof(T).GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);

        if (!Array.Exists(ctors, (ci) => ci.GetParameters().Length == 0))
        {
            throw new InvalidOperationException("Non-public ctor() was not found.");
        }

        var ctor = Array.Find(ctors, (ci) => ci.GetParameters().Length == 0);

        return ctor.Invoke(new object[] { }) as T;
    }, LazyThreadSafetyMode.ExecutionAndPublication);

    public static T Instance
    {
        get { return instance ?? (instance = Lazy.Value); }
        set { instance = value; }
    }
}

