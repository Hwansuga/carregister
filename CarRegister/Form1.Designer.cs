﻿namespace CarRegister
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.button_Test = new System.Windows.Forms.Button();
            this.button_Stop = new System.Windows.Forms.Button();
            this.button_Start = new System.Windows.Forms.Button();
            this.tableLayoutPanel_Option = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDown_CntAcc = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_CntGoal = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Delay = new System.Windows.Forms.NumericUpDown();
            this.button_Ready = new System.Windows.Forms.Button();
            this.button_CreatePage = new System.Windows.Forms.Button();
            this.comboBox_AcNo = new System.Windows.Forms.ComboBox();
            this.comboBox_Kind = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label_TopItem = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.richTextBox_Log = new System.Windows.Forms.RichTextBox();
            this.panel_Option = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel_Option.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_CntAcc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_CntGoal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Delay)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel_Option.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel1.Controls.Add(this.button_Test);
            this.panel1.Controls.Add(this.button_Stop);
            this.panel1.Controls.Add(this.button_Start);
            this.panel1.Controls.Add(this.tableLayoutPanel_Option);
            this.panel1.Controls.Add(this.button_Ready);
            this.panel1.Controls.Add(this.button_CreatePage);
            this.panel1.Controls.Add(this.comboBox_AcNo);
            this.panel1.Controls.Add(this.comboBox_Kind);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(166, 450);
            this.panel1.TabIndex = 0;
            // 
            // button_Test
            // 
            this.button_Test.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button_Test.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_Test.Location = new System.Drawing.Point(0, 148);
            this.button_Test.Name = "button_Test";
            this.button_Test.Size = new System.Drawing.Size(166, 27);
            this.button_Test.TabIndex = 4;
            this.button_Test.Text = "TEST";
            this.button_Test.UseVisualStyleBackColor = false;
            this.button_Test.Click += new System.EventHandler(this.button_Test_Click);
            // 
            // button_Stop
            // 
            this.button_Stop.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button_Stop.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_Stop.Location = new System.Drawing.Point(0, 121);
            this.button_Stop.Name = "button_Stop";
            this.button_Stop.Size = new System.Drawing.Size(166, 27);
            this.button_Stop.TabIndex = 2;
            this.button_Stop.Text = "정지";
            this.button_Stop.UseVisualStyleBackColor = false;
            this.button_Stop.Click += new System.EventHandler(this.button_Stop_Click);
            // 
            // button_Start
            // 
            this.button_Start.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button_Start.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_Start.Location = new System.Drawing.Point(0, 94);
            this.button_Start.Name = "button_Start";
            this.button_Start.Size = new System.Drawing.Size(166, 27);
            this.button_Start.TabIndex = 1;
            this.button_Start.Text = "시작";
            this.button_Start.UseVisualStyleBackColor = false;
            this.button_Start.Click += new System.EventHandler(this.button_Start_Click);
            // 
            // tableLayoutPanel_Option
            // 
            this.tableLayoutPanel_Option.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tableLayoutPanel_Option.ColumnCount = 2;
            this.tableLayoutPanel_Option.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 56.0241F));
            this.tableLayoutPanel_Option.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43.9759F));
            this.tableLayoutPanel_Option.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel_Option.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel_Option.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel_Option.Controls.Add(this.numericUpDown_CntAcc, 1, 0);
            this.tableLayoutPanel_Option.Controls.Add(this.numericUpDown_CntGoal, 1, 1);
            this.tableLayoutPanel_Option.Controls.Add(this.numericUpDown_Delay, 1, 2);
            this.tableLayoutPanel_Option.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel_Option.Location = new System.Drawing.Point(0, 365);
            this.tableLayoutPanel_Option.Name = "tableLayoutPanel_Option";
            this.tableLayoutPanel_Option.RowCount = 3;
            this.tableLayoutPanel_Option.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel_Option.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel_Option.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel_Option.Size = new System.Drawing.Size(166, 85);
            this.tableLayoutPanel_Option.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 28);
            this.label1.TabIndex = 0;
            this.label1.Text = "브라우져 수";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 28);
            this.label2.TabIndex = 1;
            this.label2.Text = "목표등록수";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 29);
            this.label3.TabIndex = 2;
            this.label3.Text = "대기시간(초)";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numericUpDown_CntAcc
            // 
            this.numericUpDown_CntAcc.Location = new System.Drawing.Point(96, 3);
            this.numericUpDown_CntAcc.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown_CntAcc.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_CntAcc.Name = "numericUpDown_CntAcc";
            this.numericUpDown_CntAcc.Size = new System.Drawing.Size(67, 21);
            this.numericUpDown_CntAcc.TabIndex = 3;
            this.numericUpDown_CntAcc.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericUpDown_CntGoal
            // 
            this.numericUpDown_CntGoal.Location = new System.Drawing.Point(96, 31);
            this.numericUpDown_CntGoal.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDown_CntGoal.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_CntGoal.Name = "numericUpDown_CntGoal";
            this.numericUpDown_CntGoal.Size = new System.Drawing.Size(67, 21);
            this.numericUpDown_CntGoal.TabIndex = 4;
            this.numericUpDown_CntGoal.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // numericUpDown_Delay
            // 
            this.numericUpDown_Delay.Location = new System.Drawing.Point(96, 59);
            this.numericUpDown_Delay.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown_Delay.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown_Delay.Name = "numericUpDown_Delay";
            this.numericUpDown_Delay.Size = new System.Drawing.Size(67, 21);
            this.numericUpDown_Delay.TabIndex = 5;
            this.numericUpDown_Delay.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // button_Ready
            // 
            this.button_Ready.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button_Ready.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_Ready.Location = new System.Drawing.Point(0, 67);
            this.button_Ready.Name = "button_Ready";
            this.button_Ready.Size = new System.Drawing.Size(166, 27);
            this.button_Ready.TabIndex = 3;
            this.button_Ready.Text = "준비";
            this.button_Ready.UseVisualStyleBackColor = false;
            this.button_Ready.Click += new System.EventHandler(this.button_Ready_Click);
            // 
            // button_CreatePage
            // 
            this.button_CreatePage.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button_CreatePage.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_CreatePage.Location = new System.Drawing.Point(0, 40);
            this.button_CreatePage.Name = "button_CreatePage";
            this.button_CreatePage.Size = new System.Drawing.Size(166, 27);
            this.button_CreatePage.TabIndex = 0;
            this.button_CreatePage.Text = "창 생성";
            this.button_CreatePage.UseVisualStyleBackColor = false;
            this.button_CreatePage.Click += new System.EventHandler(this.button_CreatePage_Click);
            // 
            // comboBox_AcNo
            // 
            this.comboBox_AcNo.Dock = System.Windows.Forms.DockStyle.Top;
            this.comboBox_AcNo.FormattingEnabled = true;
            this.comboBox_AcNo.Location = new System.Drawing.Point(0, 20);
            this.comboBox_AcNo.Name = "comboBox_AcNo";
            this.comboBox_AcNo.Size = new System.Drawing.Size(166, 20);
            this.comboBox_AcNo.TabIndex = 6;
            this.comboBox_AcNo.Visible = false;
            // 
            // comboBox_Kind
            // 
            this.comboBox_Kind.Dock = System.Windows.Forms.DockStyle.Top;
            this.comboBox_Kind.FormattingEnabled = true;
            this.comboBox_Kind.Location = new System.Drawing.Point(0, 0);
            this.comboBox_Kind.Name = "comboBox_Kind";
            this.comboBox_Kind.Size = new System.Drawing.Size(166, 20);
            this.comboBox_Kind.TabIndex = 5;
            this.comboBox_Kind.SelectedIndexChanged += new System.EventHandler(this.comboBox_Kind_SelectedIndexChanged);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel2.Controls.Add(this.label_TopItem);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(306, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(484, 27);
            this.panel2.TabIndex = 1;
            // 
            // label_TopItem
            // 
            this.label_TopItem.AutoSize = true;
            this.label_TopItem.Location = new System.Drawing.Point(133, 7);
            this.label_TopItem.Name = "label_TopItem";
            this.label_TopItem.Size = new System.Drawing.Size(17, 12);
            this.label_TopItem.TabIndex = 3;
            this.label_TopItem.Text = "...";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(36, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "최상단 아이템 : ";
            // 
            // richTextBox_Log
            // 
            this.richTextBox_Log.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox_Log.Location = new System.Drawing.Point(306, 27);
            this.richTextBox_Log.Name = "richTextBox_Log";
            this.richTextBox_Log.Size = new System.Drawing.Size(484, 423);
            this.richTextBox_Log.TabIndex = 2;
            this.richTextBox_Log.Text = "";
            // 
            // panel_Option
            // 
            this.panel_Option.Controls.Add(this.panel3);
            this.panel_Option.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_Option.Location = new System.Drawing.Point(166, 0);
            this.panel_Option.Name = "panel_Option";
            this.panel_Option.Size = new System.Drawing.Size(140, 450);
            this.panel_Option.TabIndex = 3;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panel3.Controls.Add(this.label5);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(140, 41);
            this.panel3.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(54, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "옵션";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(790, 450);
            this.Controls.Add(this.richTextBox_Log);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel_Option);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Car Register";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel_Option.ResumeLayout(false);
            this.tableLayoutPanel_Option.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_CntAcc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_CntGoal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Delay)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel_Option.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_Option;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDown_CntAcc;
        private System.Windows.Forms.NumericUpDown numericUpDown_CntGoal;
        private System.Windows.Forms.NumericUpDown numericUpDown_Delay;
        private System.Windows.Forms.Button button_Stop;
        private System.Windows.Forms.Button button_Start;
        private System.Windows.Forms.Button button_CreatePage;
        private System.Windows.Forms.Button button_Ready;
        private System.Windows.Forms.Label label_TopItem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button_Test;
        private System.Windows.Forms.ComboBox comboBox_Kind;
        private System.Windows.Forms.RichTextBox richTextBox_Log;
        private System.Windows.Forms.ComboBox comboBox_AcNo;
        private System.Windows.Forms.Panel panel_Option;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label5;
    }
}

