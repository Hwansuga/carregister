﻿using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


public class PageCha : DriverController
{
    string loginUrl = "https://www.kbchachacha.com/public/login.kbc";
    string registUrl = "https://www.kbchachacha.com/secured/car/regist.kbc";

    WebClient wc = new WebClient();

    public void Login()
    {
        CreatDriver(disableImg_ : false);
        driver.Manage().Window.Size = new Size(1800, 1800);
        driver.Navigate().GoToUrl(loginUrl);
        //driver.Manage().Window.Size = new System.Drawing.Size(1600, 1200);

        Thread.Sleep(1000);
        driver.FindElementByXPath("//*[@id='tabs']/li[2]").Click();


        wc.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36");
        //wc.Headers.Add("Content-Type", "application/json, charset=UTF-8");
        wc.Headers.Add("Accept", " */*");
        wc.Headers.Add("Host", "www.kbchachacha.com");
        wc.Headers.Add("origin", "https://www.kbchachacha.com");
        wc.Headers.Add("Referer", "https://www.kbchachacha.com/secured/car/regist.kbc");
        wc.Headers.Add("Sec-Fetch-Dest", "empty");
        wc.Headers.Add("Sec-Fetch-Mode", "cors");
        wc.Headers.Add("Sec-Fetch-Site", "same-origin");
        wc.Headers.Add("Accept-Encoding", "gzip, deflate, br");
        wc.Headers.Add("Accept-Language", "ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7");
        wc.Headers.Add("X-AJAX", "true");
        wc.Headers.Add("X-Requested-With", "XMLHttpRequest");
    }

    public void MoveRegPage()
    {
        driver.Navigate().GoToUrl(registUrl);
        Thread.Sleep(500);
    }

    public void SetHeader(WebClient wc_, string key_, string value_)
    {
        if (wc_.Headers.AllKeys.Contains(key_))
            wc_.Headers[key_] = value_;
        else
            wc_.Headers.Add(key_, value_);
    }

    public void CollectCookie()
    {
        if (driver == null)
            return;

        var myCookies = driver.Manage().Cookies.AllCookies;
        List<string> listCookies = new List<string>();
        foreach (OpenQA.Selenium.Cookie itemCookie in myCookies)
        {
            listCookies.Add($"{itemCookie.Name}={itemCookie.Value}");
        }

        wc.Headers.Remove(HttpRequestHeader.Cookie);
        wc.Headers.Add(HttpRequestHeader.Cookie, string.Join(";", listCookies));

        Thread.Sleep(100);
        //QuiteDriver();
    }

    public void SetPicture(string path_, int cnt_)
    {
        var fullPath = AppDomain.CurrentDomain.BaseDirectory + path_;
        if (Directory.Exists(fullPath) == false)
            return;
        var files = Directory.GetFiles(fullPath);

        while(files.Length != cnt_)
        {
            files = Directory.GetFiles(fullPath);
            Thread.Sleep(100);
        }


        string typ1 = string.Join(" ", files);
        string typ2 = $"\"{string.Join("\" \"", files)}\"";
        string typ3 = $"{string.Join(" \n ", files)}";
        string typ4 = $"{string.Join("\n ", files)}";
        string typ5 = $"{string.Join("\n", files)}";

        var listFiles = files.ToList();    
        Global.uiController.PrintLog($"listFiles : {listFiles.Count}");
        listFiles = listFiles.OrderBy(x => int.Parse(Path.GetFileNameWithoutExtension(x))).ToList();
        //Util.SortByNameNymber(listFiles);

        //*[@id="CarPhoto"]/ul
        //*[@id="CarPhoto"]/ul/li[1]

        while (true)
        {
            driver.ExecuteScript("window.focus();");
            Thread.Sleep(1000);

            driver.FindElementById("btnPhotoDeleteAll").Click();
            Thread.Sleep(2000);

            driver.FindElementById("layerConfirmBtnOk").Click();
            Thread.Sleep(2000);

            var btnImage = driver.FindElementById("frmImageUpload").FindElement(By.Id("file1"));
            driver.ExecuteScript("uploadPhoto.onAttachMultiFile();");
            Thread.Sleep(2000);

            foreach(var item in listFiles)
            {
                btnImage = driver.FindElementById("frmImageUpload").FindElement(By.Id("file1"));
                btnImage.SendKeys(item);
                Thread.Sleep(2000);
            }

            //btnImage.SendKeys(typ4);
            //Thread.Sleep(1000 + files.Length * 500);

            driver.ExecuteScript("window.focus();");
            Thread.Sleep(1000);

            var listPhoto = driver.FindElementById("CarPhoto").FindElements(By.XPath("./ul/li")).ToList();
            listPhoto = listPhoto.FindAll(x => x.GetAttribute("isdata") == "true");            

            if (listPhoto.Count >= cnt_)
                break;

        }
                     
        //driver.FindElementById("btnPhotoUploadAll").Click();
        //Thread.Sleep(1000);
        

        //for(int i=0; i< files.Length; ++i)
        //{
        //    //var btnImage = driver.FindElementById("frmImageUpload").FindElement(By.Id($"file1"));
        //    btnImage.SendKeys(files[i]);
        //    Thread.Sleep(1500);
        //    driver.ExecuteScript("uploadPhoto.onAttachMultiFile();");
        //    Thread.Sleep(500);
        //}
    }

    bool CheckValidNo(string item_)
    {
        driver.Navigate().GoToUrl(registUrl);
        Thread.Sleep(500);

        string strPosOrder = $"window.scroll(0,0)";
        driver.ExecuteScript(strPosOrder, "");
        Thread.Sleep(100);

        driver.FindElementById("selfCarNo").SendKeys(item_);
        Thread.Sleep(1000);
        driver.FindElementById("btnStep1CarNoSearch").Click();
        Thread.Sleep(1000);

        if (driver.PageSource.Contains("layerback_alert"))
        {
            var layerback_alert = driver.FindElementById("layerback_alert");
            while (IsContainAsHtml(layerback_alert, "none;") == false)
                break;
        }
        else
        {
            Thread.Sleep(1000);
        }
        

        var check = driver.FindElementById("labelStep0CarNo").Text.Trim();

        if (string.IsNullOrEmpty(check) == false)
            return true;
        else
            return false;
    }

    void ClickConfirm()
    {
        string strPosOrder = $"window.scroll(0,0)";
        driver.ExecuteScript(strPosOrder, "");
        Thread.Sleep(1000);

        driver.FindElementById("btnStepProcessNext").Click();
        Thread.Sleep(1000);

        driver.FindElementById("layerConfirmBtnOk").Click();
        Thread.Sleep(1000);
    }

    public JObject CheckNo(string item_)
    {
        var values = new System.Collections.Specialized.NameValueCollection();
        values.Add("memberNo", "0");
        values.Add("carNo", item_);

        var ret = wc.UploadValues("https://www.kbchachacha.com/secured/car/carno/check.json", "POST", values);
        var retString = Encoding.UTF8.GetString(ret);

        return JObject.Parse(retString);
    }

    public JObject DelStep(JObject checkNo_)
    {
        var values = new System.Collections.Specialized.NameValueCollection();
        values.Add("carRequestSeq", checkNo_["carRequestTempInfo"]["carRequestSeq"].ToString());

        var ret = wc.UploadValues("https://www.kbchachacha.com/secured/car/regist/request/delete.json", "POST", values);
        var retString = Encoding.UTF8.GetString(ret);

        return JObject.Parse(retString);
    }

    public JObject Step1(string item_,JObject checkNo_)
    {
        var values = new System.Collections.Specialized.NameValueCollection();
        values.Add("memberNo", "0");
        values.Add("carNo", item_);
        values.Add("regiDay", checkNo_["wbResult"]["frst_regist_de"].ToString().Substring(0,6));
        values.Add("wbSeq", checkNo_["wbResult"]["wbSeq"].ToString());
        values.Add("carHistorySeq", checkNo_["carHistorySeq"].ToString());
        values.Add("makerCode", checkNo_["wbCarResult"]["makerCode"].ToString());
        values.Add("classCode", checkNo_["wbCarResult"]["classCode"].ToString());
        values.Add("carCode", checkNo_["wbCarResult"]["carCode"].ToString());
        values.Add("modelCode", checkNo_["wbCarResult"]["modelCode"].ToString());
        values.Add("gradeCode", checkNo_["wbCarResult"]["gradeCode"].ToString());
        values.Add("carOption", "");

        var ret = wc.UploadValues("https://www.kbchachacha.com/secured/car/regist/step1/save.json", "POST", values);
        var retString = Encoding.UTF8.GetString(ret);

        return JObject.Parse(retString);
    }

    public JObject Step2(JObject checkNo_ , JObject step1_)
    {
        var values = new System.Collections.Specialized.NameValueCollection();
        values.Add("carRequestSeq", step1_["carRequestSeq"].ToString());
        values.Add("makerCode", checkNo_["wbCarResult"]["makerCode"].ToString());
        values.Add("classCode", checkNo_["wbCarResult"]["classCode"].ToString());
        values.Add("carCode", checkNo_["wbCarResult"]["carCode"].ToString());
        values.Add("modelCode", checkNo_["wbCarResult"]["modelCode"].ToString());
        values.Add("gradeCode", checkNo_["wbCarResult"]["gradeCode"].ToString());

        var ret = wc.UploadValues("https://www.kbchachacha.com/secured/car/regist/step2/save.json", "POST", values);
        var retString = Encoding.UTF8.GetString(ret);

        return JObject.Parse(retString);
    }

    public JObject GetStep3Data(JObject carData_ , JObject checkNo_)
    {
        var values = new System.Collections.Specialized.NameValueCollection();
        values.Add("carRequestSeq", carData_["carDetail"]["carRequestSeq"].ToString());
        values.Add("wbSeq", checkNo_["wbResult"]["wbSeq"].ToString());

        var ret = wc.UploadValues("https://www.kbchachacha.com/secured/car/regist/step3/data.json", "POST", values);
        var retString = Encoding.UTF8.GetString(ret);

        return JObject.Parse(retString);
    }

    public JObject Step3(JObject carData_ , JObject dataStep3_ , CarRegiInfo carInfo_ = null)
    {
        var dataValues = new System.Collections.Specialized.NameValueCollection();
        dataValues.Add("carRequestSeq", carData_["carDetail"]["carRequestSeq"].ToString());
        //dataValues.Add("wbSeq", checkNo_["wbResult"]["wbSeq"].ToString());

        var values = new System.Collections.Specialized.NameValueCollection();
        values.Add("carRequestSeq", carData_["carDetail"]["carRequestSeq"].ToString());
        values.Add("regiDay", carData_["carDetail"]["regiDay"].ToString());
        values.Add("yymm", dataStep3_["wbDetail"]["prye"].ToString());                        
        
        
        values.Add("distraintInfo", carData_["carDetail"]["distraintInfo"].ToString());
        values.Add("mortgageInfo", carData_["carDetail"]["mortgageInfo"].ToString());
        values.Add("taxDefaultInfo", carData_["carDetail"]["taxDefaultInfo"].ToString());
        values.Add("takeMen", "215130");
        
        values.Add("colorName", "");
        values.Add("interestFreeYn", "N");
        values.Add("falsityYn", "Y");
        values.Add("warrantyYn", "N");

        if (carInfo_ == null)
        {
            values.Add("gas", GetFuelCode(""));
            values.Add("autoGbn", GetGearCode(""));
            values.Add("color", GetColorCode(""));
            values.Add("km", "50000");
            values.Add("numCc", "1");
            values.Add("jesiNo", "1");
        }
        else
        {
            values.Add("gas", GetFuelCode(carInfo_.c_fuel));
            values.Add("autoGbn", GetGearCode(carInfo_.c_gearbox));
            values.Add("color", GetColorCode(carInfo_.c_colorName));

            values.Add("km", carInfo_.c_mileage);
            values.Add("numCc", carInfo_.c_displacement);
            values.Add("jesiNo", carInfo_.c_inNumber);
        }

        var ret = wc.UploadValues("https://www.kbchachacha.com/secured/car/regist/step3/save.json", "POST", values);
        var retString = Encoding.UTF8.GetString(ret);

        return JObject.Parse(retString);
    }

    public JObject Step4(JObject carData_)
    {
        var values = new System.Collections.Specialized.NameValueCollection();
        values.Add("carRequestSeq", carData_["carDetail"]["carRequestSeq"].ToString());
        var data = wc.UploadValues("https://www.kbchachacha.com/secured/car/regist/step4/data.json", "POST", values);
        var dataString = Encoding.UTF8.GetString(data);
        var jData = JObject.Parse(dataString);  
        values.Add("carOption", jData["carOption"].ToString());

        var ret = wc.UploadValues("https://www.kbchachacha.com/secured/car/regist/step4/save.json", "POST", values);
        var retString = Encoding.UTF8.GetString(ret);

        return JObject.Parse(retString);
    }

    public JObject Step5(JObject carData_ , string sellAmt_)
    {
        var values = new System.Collections.Specialized.NameValueCollection();
        values.Add("carRequestSeq", carData_["carDetail"]["carRequestSeq"].ToString());

        values.Add("sellAmt", sellAmt_);
        values.Add("sellAmtGbn", "109100");
        
        values.Add("leaseGbn", "11");
        values.Add("takeOverAmt", carData_["carDetail"]["takeOverAmt"].ToString());
        values.Add("leasePayGbn", carData_["carDetail"]["leasePayGbn"].ToString());
        values.Add("depositAmt", carData_["carDetail"]["depositAmt"].ToString());
        values.Add("monthLeaseAmt", carData_["carDetail"]["monthLeaseAmt"].ToString());
        values.Add("scrapValueAmt", carData_["carDetail"]["scrapValueAmt"].ToString());
        values.Add("fromLeasePeriod", "");
        values.Add("toLeasePeriod", "");
        values.Add("remainLeasePeriod", "");

        var ret = wc.UploadValues("https://www.kbchachacha.com/secured/car/regist/step5/save.json", "POST", values);
        var retString = Encoding.UTF8.GetString(ret);

        return JObject.Parse(retString);
    }

    public JObject GetStep5Data(JObject carData_)
    {
        var values = new System.Collections.Specialized.NameValueCollection();
        values.Add("carRequestSeq", carData_["carDetail"]["carRequestSeq"].ToString());

        var ret = wc.UploadValues("https://www.kbchachacha.com/secured/car/regist/step5/data.json", "POST", values);
        var retString = Encoding.UTF8.GetString(ret);

        return JObject.Parse(retString);
    }

    public JObject Step6(JObject carData_ , List<string> listImg_ = null)
    {

        var values = new System.Collections.Specialized.NameValueCollection();
        values.Add("carRequestSeq", carData_["carDetail"]["carRequestSeq"].ToString());

        if (listImg_ == null)
        {
            values.Add($"photo1", "12258023018109717.jpg");
            values.Add($"photo2", "12258023106567009.jpg");
            values.Add($"photo3", "12258023193462066.jpg");
            values.Add($"photo4", "12258022910182682.jpg");
            values.Add($"photo5", "12258023325893592.jpg");
            values.Add($"photo6", "12258023440859654.jpg");
            for (int i = 7; i < 33; ++i)
                values.Add($"photo{i}", "");
        }
        else
        {
            var nvc = new System.Collections.Specialized.NameValueCollection();


            int cnt = 1;
            foreach(var img in listImg_)
            {
                if (cnt >= 33)
                    break;


                values.Add($"photo{cnt}", img);
                ++cnt;
            }

            if (cnt < 33)
            {
                for (int i = cnt; i < 33; ++i)
                    values.Add($"photo{i}", "");
            }
        }
        
        var ret = wc.UploadValues("https://www.kbchachacha.com/secured/car/regist/step6/save.json", "POST", values);
        var retString = Encoding.UTF8.GetString(ret);

        return JObject.Parse(retString);
    }

    public JObject Step7(JObject carData_ , CarRegiInfo carInfo_ = null)
    {
        var values = new System.Collections.Specialized.NameValueCollection();
        values.Add("carRequestSeq", carData_["carDetail"]["carRequestSeq"].ToString());
        
        values.Add("viewType", "160120");
        
        values.Add("carCheckImage1", "");
        values.Add("carCheckImage2", "");
        values.Add("carCheckImage3", "");
        values.Add("checkImageFront", "");
        values.Add("checkImageBack", "");
        values.Add("n_carCheck", "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ");
        values.Add("n_autoGbn", "");
        values.Add("n_warrantyType", "");
        values.Add("n_specialRecordType", "");
        values.Add("n_useChangeType", "");
        values.Add("n_colorType", "");
        values.Add("n_optionType", "");
        values.Add("n_repairWheelCheck", "");
        values.Add("n_repairTireCheck", "");
        values.Add("n_baseItemKeepCheck", "");
        values.Add("n_carCheckStructureA", "");
        values.Add("n_carCheckStructureB", "");
        values.Add("n_carCheckStructureC", "");
        
        
        values.Add("n_checkStrDay", "20190918");
        values.Add("n_checkEndDay", "20210917");
        values.Add("n_regiDay", carData_["carDetail"]["regiDay"].ToString());
        values.Add("n_frameNo", "");
        values.Add("n_motorForm", "");
        values.Add("n_km", "1");
        values.Add("n_chkCo", "on");
        values.Add("n_chkHc", "on");
        values.Add("n_chkPpm", "on");
        values.Add("n_co", "");
        values.Add("n_hc", "");
        values.Add("n_ppm", "");
        values.Add("n_sagoGbn", "없음");
        values.Add("n_simpleRepairGbn", "없음");
        values.Add("n_carCheckRankGbn1", "없음");
        values.Add("n_carCheckRankGbn2", "없음");
        values.Add("n_carCheckStructure", "없음");
        values.Add("n_etc", "");
        values.Add("n_amtEmpRem", "");
        values.Add("n_checkDay", "");
        values.Add("n_checkEmpName", "");
        values.Add("n_memberName", "");
        values.Add("n_checkEmpWarrantyDay", "");
        values.Add("n_checkEmpWarrantyKm", "");
        values.Add("n_checkAmtWarrantyDay", "");
        values.Add("n_checkAmtWarrantyKm", "");

        if (carInfo_ == null)
        {
            values.Add("checkNo", "1");
            values.Add("txtCheckNo", "1");

            values.Add("checkLinkUrl", "1");
            values.Add("txtCheckLinkUrl", "1");
        }
        else
        {
            values.Add("checkNo", carInfo_.c_checkNum);
            values.Add("txtCheckNo", carInfo_.c_checkNum);

            values.Add("checkLinkUrl", carInfo_.c_checkUrl);
            values.Add("txtCheckLinkUrl", carInfo_.c_checkUrl);
        }

        var ret = wc.UploadValues("https://www.kbchachacha.com/secured/car/regist/step7/save.json", "POST", values);
        var retString = Encoding.UTF8.GetString(ret);

        return JObject.Parse(retString);
    }

    public JObject Step8(JObject carData_)
    {
        var values = new System.Collections.Specialized.NameValueCollection();
        values.Add("carRequestSeq", carData_["carDetail"]["carRequestSeq"].ToString());
        values.Add($"carDesc", Global.desc);
          
        var ret = wc.UploadValues("https://www.kbchachacha.com/secured/car/regist/step8/save.json", "POST", values);
        var retString = Encoding.UTF8.GetString(ret);

        return JObject.Parse(retString);
    }

    public JObject Step9(JObject carData_)
    {
        var values = new System.Collections.Specialized.NameValueCollection();
        values.Add("carRequestSeq", carData_["carDetail"]["carRequestSeq"].ToString());
        values.Add("autoUpdateKbCoinYn", "N");
        values.Add("specialKbCoinYn", "N");
        values.Add("hotMarkKbCoinYn", "N");

        var ret = wc.UploadValues("https://www.kbchachacha.com/secured/car/regist/step9/save.json", "POST", values);
        var retString = Encoding.UTF8.GetString(ret);

        return JObject.Parse(retString);
    }

    public JObject CheckException(JObject carData_ , string sellAmt_)
    {
        var values = new System.Collections.Specialized.NameValueCollection();
        values.Add("carRequestSeq", carData_["carDetail"]["carRequestSeq"].ToString());
        values.Add("sellAmt", sellAmt_);

        var ret = wc.UploadValues("https://www.kbchachacha.com/secured/car/regist/exception/check.json", "POST", values);
        var retString = Encoding.UTF8.GetString(ret);

        return JObject.Parse(retString);
    }

    public JObject RegFinish(JObject carData_)
    {
        var values = new System.Collections.Specialized.NameValueCollection();
        values.Add("carRequestSeq", carData_["carDetail"]["carRequestSeq"].ToString());
        values.Add("adState", "030160");
        values.Add("explainRemark", "");
        values.Add("explainImage1", "");
        values.Add("explainImage2", "");

        var ret = wc.UploadValues("https://www.kbchachacha.com/secured/car/regist/request/save.json", "POST", values);
        var retString = Encoding.UTF8.GetString(ret);

        return JObject.Parse(retString);
    }


    public JObject CheckSeq(string carNo)
    {
        var values = new System.Collections.Specialized.NameValueCollection();
        values.Add("gotoPage", "1");
        values.Add("pageSize", "100");
        values.Add("makerCode", "");
        values.Add("classCode", "");
        values.Add("fromDay", "");
        values.Add("carNo", carNo);
        values.Add("searchOrder", "adDayCount");
        values.Add("searchOrderType", "DESC");

        var ret = wc.UploadValues("https://www.kbchachacha.com/secured/mydealer/car/mycar/dealer/list01.json", "POST", values);
        var retString = Encoding.UTF8.GetString(ret);

        return JObject.Parse(retString);
    }

    public JObject RegWait(string carNo)
    {
        var retCheckReg = CheckSeq(carNo);
        var carSeq = retCheckReg["list"][0]["carSeq"].ToString();

        var values = new System.Collections.Specialized.NameValueCollection();
        values.Add("carSeq", carSeq);
        var ret = wc.UploadValues("https://www.kbchachacha.com/secured/mydealer/car/mycar/state/wait/regist.json", "POST", values);
        var retString = Encoding.UTF8.GetString(ret);

        return JObject.Parse(retString);
    }

    public JObject RegWaitBySeq(string carSeq)
    {
        var values = new System.Collections.Specialized.NameValueCollection();
        values.Add("carSeq", carSeq);
        var ret = wc.UploadValues("https://www.kbchachacha.com/secured/mydealer/car/mycar/state/wait/regist.json", "POST", values);
        var retString = Encoding.UTF8.GetString(ret);

        return JObject.Parse(retString);
    }



    public string curCartSeq = "";
    public bool RegiItemByHttp(string item_)
    {
        var checkNo = CheckNo(item_);
        if (checkNo["errCode"].ToString() == "8")
        {
            var del = this.DelStep(checkNo);
            checkNo = CheckNo(item_);
        }

        if (checkNo["errCode"].ToString() != "0")
            return false;

        Global.uiController.PrintLog($"{item_} 등록 시도");

        var step1 = Step1(item_, checkNo);
        if (string.IsNullOrEmpty(step1["errMsg"].ToString()) == false)
            return false;

        curCartSeq = step1["carRequestSeq"].ToString();

        var step2 = Step2(checkNo, step1);
        if (step2["errCode"].ToString() != "0")
            return false;

        var dateStep3 = this.GetStep3Data(step1, checkNo);
        var step3 = Step3(step1, dateStep3);
        if (step3["errCode"].ToString() != "0")
            return false;
        
        var step4 = this.Step4(step1);
        if (step4["errCode"].ToString() != "0")
            return false;

        var step5 = this.Step5(step1, "20000");
        if (step5["errCode"].ToString() != "0")
            return false;

        var step6 = this.Step6(step1);
        if (step6["errCode"].ToString() != "0")
            return false;
        var step7 = this.Step7(step1);
        if (step7["errCode"].ToString() != "0")
            return false;
        var step8 = this.Step8(step1);
        if (step8["errCode"].ToString() != "0")
            return false;
        var step9 = this.Step9(step1);
        if (step9["errCode"].ToString() != "0")
            return false;
        //                         var checkException = this.CheckException(step1 , marketPrice);
        //                         if (checkException["errCode"].ToString() == "0")
        
        //                             var seq = step1["carDetail"]["carRequestSeq"].ToString();
        //                             var adState = step1["carDetail"]["adState"].ToString();
        // 
        //                             string script = 
        //                                 "var carRequestSeq = setParams.setCarRequestSeq.getValue();"
        //                                 + "var adState = '030160';"
        //                                 + "var explainRemark = '';"
        //                                 + "var explainImage1 = '';"
        //                                 + "var explainImage2 = '';"
        //                                 + "var params = [];"
        //                                 + "params.push({ name: 'carRequestSeq',value: " + seq + "});" 
        //                                 + "params.push({ name: 'adState',value:adState});"
        //                                 + "params.push({ name: 'explainRemark',value:explainRemark});"
        //                                 + "params.push({ name: 'explainImage1',value:explainImage1});"
        //                                 + "params.push({ name: 'explainImage2',value:explainImage2});"
        //                                 + "$_COMMON.callAjax({"
        //                                 + "url: '/secured/car/regist/request/save.json',params:params,success:" 
        //                                 + "function(data){var errCode = data.errCode;var errMsg = data.errMsg;var memberGbnCode2 = data.memberGbnCode2;}})";
        // 
        //                             driver.ExecuteScript(script);

        var finish = this.RegFinish(step1);
        if (finish["errCode"].ToString() == "0")
            return true;
               
        return false;
    }
    
    public void GetLog()
    {
        var logs = driver.Manage().Logs.GetLog("driver");
    }

    string GetFuelCode(string fuel_)
    {
        switch (fuel_)
        {
            case "가솔린":
            case "휘발유":
                return "004001";
            case "디젤":
            case "경유":
                return "004002";
            case "LPG":
                return "004003";
            case "가솔린+LPG":
                return "004010";
            case "하이브리드(LPG)":
                return "004004";
            case "하이브리드(가솔린)":
                return "004005";
            case "하이브리드(디젤)":
                return "004011";
            case "CNG":
                return "004006";
            case "전기":
                return "004007";
            default:
                return "004008";
        }
    }

    string GetColorCode(string color_)
    {
        switch (color_)
        {
            case "검정":
            case "검정색":
                return "006001";
            case "흰색":
                return "006002";
            case "은색":
                return "006003";
            case "진주":
            case "진주색":
                return "006004";
            case "회색":
                return "006005";
            case "빨강색":
            case "빨강":
                return "006006";
            case "파랑색":
            case "파랑":
                return "006007";
            case "주황색":
            case "주황":
                return "006008";
            case "갈색":
                return "006009";
            case "초록색":
            case "초록":
                return "006010";
            case "노랑색":
            case "노랑":
                return "006011";
            case "보라색":
            case "보라":
                return "006012";
            default:
                return "006001";
        }
    }

    string GetGearCode(string gear_)
    {
        switch (gear_)
        {
            case "수동":
                return "005001";
            case "오토":
                return "005002";
            case "SAT":
                return "005003";
            case "CVT":
                return "005004";
            default:
                return "005005";
        }
    }

    public bool RegiCar(JObject jObj_ , CarRegiInfo carInfo_)
    {
        var step1 = Step1(carInfo_.num, jObj_);
        if (string.IsNullOrEmpty(step1["errMsg"].ToString()) == false)
            return false;

        curCartSeq = step1["carRequestSeq"].ToString();

        var step2 = Step2(jObj_, step1);
        if (step2["errCode"].ToString() != "0")
            return false;

        var dateStep3 = this.GetStep3Data(step1, jObj_);
        var step3 = Step3(step1, dateStep3 , carInfo_);
        if (step3["errCode"].ToString() != "0")
            return false;

        var step4 = this.Step4(step1);
        if (step4["errCode"].ToString() != "0")
            return false;

        var step5 = this.Step5(step1, carInfo_.price);
        if (step5["errCode"].ToString() != "0")
            return false;

        var step6 = this.Step6(step1);
        if (step6["errCode"].ToString() != "0")
            return false;
        var step7 = this.Step7(step1 , carInfo_);
        if (step7["errCode"].ToString() != "0")
            return false;
        var step8 = this.Step8(step1);
        if (step8["errCode"].ToString() != "0")
            return false;
        var step9 = this.Step9(step1);
        if (step9["errCode"].ToString() != "0")
            return false;

        var finish = this.RegFinish(step1);
        if (finish["errCode"].ToString() != "0")
        {
#if DEBUG
            Global.uiController.PrintLog($"{carInfo_.num} - {finish["errMsg"].ToString()}" , Color.Red);
#endif
            return false;
        }
           
        return true;
    }

    public JObject GetTempData()
    {
        var values = new System.Collections.Specialized.NameValueCollection();
        values.Add("gotoPage", "1");
        values.Add("pageSize", "100");
        values.Add("makerCode", "");
        values.Add("classCode", "");
        values.Add("fromDay", "");
        values.Add("toDay", "");
        values.Add("carNo", $"");
        values.Add("searchOrder", $"regiDate");
        values.Add("searchOrderType", $"DESC");

        var ret = wc.UploadValues("https://www.kbchachacha.com/secured/mydealer/car/mycar/dealer/list08.json", "POST", values);
        var retString = Encoding.UTF8.GetString(ret);

        return JObject.Parse(retString);
    }

    public JObject DelTempData(string carRequestSeq_)
    {
        var values = new System.Collections.Specialized.NameValueCollection();
        values.Add("carRequestSeq", carRequestSeq_);

        var ret = wc.UploadValues("https://www.kbchachacha.com/secured/mydealer/car/mycar/request/delete/regist.json", "POST", values);
        var retString = Encoding.UTF8.GetString(ret);

        return JObject.Parse(retString);
    }

    public JObject List01(string num_)
    {
        var values = new System.Collections.Specialized.NameValueCollection();
        values.Add("gotoPage", "1");
        values.Add("pageSize", "100");
        values.Add("makerCode", "");
        values.Add("classCode", "");
        values.Add("fromDay", "");
        values.Add("toDay", "");
        values.Add("carNo", $"{num_}");
        values.Add("searchOrder", $"adDayCount");
        values.Add("searchOrderType", $"DESC");

        var ret = wc.UploadValues("https://www.kbchachacha.com/secured/mydealer/car/mycar/dealer/list01.json", "POST", values);
        var retString = Encoding.UTF8.GetString(ret);

        return JObject.Parse(retString);
    }

    public string ProcessPhotoUpload(CarRegiInfo info_)
    {
        var list01 = List01(info_.num);
        var seq = list01["list"][0]["carSeq"].ToString();

        try
        {
            Global.uiController.PrintLog($"{info_.num} 차량 편집 페이지 이동 {seq}");
            driver.Navigate().GoToUrl($"https://www.kbchachacha.com/secured/car/modify.kbc?carSeq={seq}");
            Thread.Sleep(2000);
            //*[@id="btnStep6Select"]
            driver.FindElementById("btnStep6Select").Click();
            Thread.Sleep(2000);



            WebClient wc = new WebClient();
            int i = 0;
            foreach (var img in info_.listImg)
            {
                string name = $@"Img\{info_.num}\{i}.png";
                if (File.Exists(name))
                    File.Delete(name);
                var fileInfo = new FileInfo(name);
                fileInfo.Directory.Create();
                wc.DownloadFile(img, name);
                Thread.Sleep(100);
                i++;
            }

            //if (i < 7)
            //{
            //    for(int k=i; k<7; ++k)
            //    {
            //        string name = $@"Img\{info_.num}\{k}.png";
            //        if (File.Exists(name))
            //            File.Delete(name);
            //        var fileInfo = new FileInfo(name);
            //        fileInfo.Directory.Create();
            //        wc.DownloadFile("https://search.pstatic.net/common/?src=http%3A%2F%2Fblogfiles.naver.net%2FMjAyMTAyMTdfMTc0%2FMDAxNjEzNTYwMDQ5Njg5.ayJq6bbi-tm0fEeMIhjph5r7OaGZ0Fhqz9K2tjyZ9pQg.aRSoCt6ULr39IhjuCa9kINZU_p0SD07CGgql3aV2otMg.JPEG.vamtol3%2Fsunny.jpg&type=sc960_832", name);
            //    }
            //}

            SetPicture($@"Img\{info_.num}", info_.listImg.Count);

            driver.FindElementById("btnStepProcessNext").Click();
            Thread.Sleep(2000);

            driver.FindElementById("layerConfirmBtnOk").Click();
            Thread.Sleep(2000);
        }
        catch(Exception ex)
        {
#if DEBUG
            Global.uiController.PrintLog($"{info_.num} 사진 업로드중 에러 발생 : {ex.Message}");
#endif
        }


        return seq;

    }
}

