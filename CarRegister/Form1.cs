﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarRegister
{
    public partial class Form1 : Form
    {
        public List<PageCha> listPageCha = new List<PageCha>();

        public PageCarmodoo car = new PageCarmodoo();

        readonly object thisLock = new object();

        ConsoleTextBoxWriter logger;

        List<CheckBox> listOption = new List<CheckBox>();

        public Form1()
        {
            InitializeComponent();
            logger = new ConsoleTextBoxWriter(richTextBox_Log);
        }

        public void PrintLog(string msg_)
        {
            logger.WriteLine(msg_);
        }

        public void PrintLog(string msg_ , Color color_)
        {
            logger.WriteLineWithColor(msg_ , color_);
        }

        public void UpdateTopItemLabel()
        {
            this.label_TopItem.Invoke(new MethodInvoker(delegate ()
            {
                this.label_TopItem.Text = Global.topItem;
            }));

            PrintLog($"현재 최상단 아이템 : {Global.topItem}");
        }

        void SetStartPosition()
        {
            StartPosition = FormStartPosition.Manual;
            Rectangle res = Screen.PrimaryScreen.Bounds;
            Location = new Point(res.Width - Size.Width, 0);
        }

        void InitState()
        {
            

#if DEBUG
            this.button_Test.Visible = true;
#else
            this.button_Test.Visible = false;
#endif
            this.button_CreatePage.Enabled = true;
            this.button_Ready.Enabled = true;
            this.button_Start.Enabled = false;
            this.button_Stop.Enabled = false;
            this.tableLayoutPanel_Option.Enabled = true;

            this.comboBox_Kind.Items.AddRange(Global.dicCodeName.Keys.ToArray());
            this.comboBox_Kind.SelectedIndex = 0;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
#if !VER2
            Global.uiController = this;
#endif

#if DEBUG
            Util.KillProcess("chromedriver");
#else
            Util.KillProcess("chrome");
#endif
            EventUtil.ConnectCustomEvent(this);
            SetStartPosition();
            this.InitState();

            Global.LoadAccInfo();

            PrintLog("카모두 프로그램을 실행 후 로그인 해주세요.");
        }

        private void button_CreatePage_Click(object sender, EventArgs e)
        {
            this.button_CreatePage.Enabled = false;

            foreach (var item in listPageCha)            
                item.QuiteDriver();

            listPageCha.Clear();

            for (int i = 0; i < Global.numericUpDown_CntAcc; ++i)
            {
                var page = new PageCha();
                page.Login();
                listPageCha.Add(page);
            }

            PrintLog("모든 페이지에 로그인 해주세요.");
        }

        void UpdateComboBoxAcNo()
        {
            comboBox_AcNo.Items.Clear();
            foreach(var item in Global.dicAcNo)
            {
                comboBox_AcNo.Items.Add(item.Key);
            }

            comboBox_AcNo.SelectedIndex = 0;
        }

        private void button_Ready_Click(object sender, EventArgs e)
        {           
            this.button_Start.Enabled = true;
            this.button_Ready.Enabled = false;

            car.QuiteDriver();
            car.Login();

            if (Global.comboBox_Kind == "수원")
                CreateOption();

            UpdateComboBoxAcNo();

            while (true)
            {
                Global.topItem = car.GetTopItem();
                if (string.IsNullOrEmpty(Global.topItem) == false)
                    break;
            }
            

            this.UpdateTopItemLabel();

            PrintLog($"{Global.comboBox_Kind} 준비 완료");
        }

        void CreateOption()
        {
            foreach (var item in panel_Option.Controls.OfType<CheckBox>().ToList())
            {
                panel_Option.Controls.Remove(item);
            }

            listOption.Clear();
            foreach (var item in Global.dicAcNo)
            {
                var checkBox = new CheckBox();
                panel_Option.Controls.Add(checkBox);

                checkBox.Text = item.Key;
                checkBox.Dock = DockStyle.Top;
                checkBox.Name = item.Key;
                checkBox.BringToFront();

                listOption.Add(checkBox);
            }
        }

        private void button_Start_Click(object sender, EventArgs e)
        {
            this.button_Start.Enabled = false;
            this.button_Stop.Enabled = true;
            this.tableLayoutPanel_Option.Enabled = false;
            Global.stop = false;

            foreach (var itemCar in listPageCha)
                itemCar.CollectCookie();
#if !DEBUG
            foreach (var itemCar in listPageCha)
                itemCar.driver.Manage().Window.Position = new Point(-10000, -10000);
#endif

            
            Thread worker = new Thread(() => {
                while (true)
                {
                    if (Global.stop)
                        break;

                    int cntReg = 0;

                    foreach (var item in listPageCha)
                        item.MoveRegPage();

                    PrintLog("시작");

                    while (Global.numericUpDown_CntGoal > cntReg)
                    {
                        //Thread.Sleep(100);
                        List<string> listTargetItem = car.GetListItemByHttp();
                        if (Global.comboBox_Kind == "수원")
                        {
                            var checkedAcNo = listOption.FindAll(x => x.Checked).Select(x => Global.dicAcNo[x.Text]).ToArray();
                            listTargetItem = car.GetListItemByHttp(100, checkedAcNo, 20).Select(x=>x.num).ToList();
                        }
                        

                        if (listTargetItem.Count <= 0)
                            continue;

                        string curTopItem = listTargetItem[0];
                        if (Global.topItem == curTopItem)
                            continue;

                        listTargetItem = listTargetItem.FindAll(x => x.Contains("허") == false && x.Contains("하") == false && x.Contains("호") == false);

                        var listSpiteData = Util.SplitList<string>(listTargetItem , listPageCha.Count);
                        //listSpiteData.Reverse();
                        foreach (var listItem in listSpiteData)
                        {
                            List<Thread> listThread = new List<Thread>();
                            for (int i = 0; i < listItem.Count; ++i)
                            {
                                var thread = new Thread(new ParameterizedThreadStart((index_) =>
                                {
                                    int index = (int)index_;
                                    bool ret = false;
                                    try
                                    {
                                        ret = listPageCha[index].RegiItemByHttp(listItem[index]);

                                    }
                                    catch
                                    {

                                    }
                                    
                                    if (ret)
                                    {
                                        cntReg += 1;
                                        PrintLog($"::{listItem[index]} 등록 성공::");
                                        var retWait = listPageCha[index].RegWait(listItem[index]);
                                        if (retWait["errCode"].ToString() == "0")
                                        {
                                            PrintLog($"::{listItem[index]} 대기 이동 성공::" , Color.Blue);
                                        }
                                        else{
                                            PrintLog($"::{listItem[index]} 대기 이동 실패::");
                                        }
                                    }
                                    else
                                    {
                                        PrintLog($"::{listItem[index]} 등록 실패::");
                                    }

                                }));
                                thread.Start(i);

                                listThread.Add(thread);
                            }

                            foreach (var item in listThread)
                                item.Join();
                        }

                        Global.topItem = curTopItem;
                        this.UpdateTopItemLabel();
                    }

                    PrintLog($"목표개수 달성({Global.numericUpDown_CntGoal}), {Global.numericUpDown_Delay}초 대기");
                    Thread.Sleep(Global.numericUpDown_Delay * 1000);
                }

                PrintLog("정지 완료");
                
                this.InitState();
            });
            worker.Start();
        }

        private void button_Stop_Click(object sender, EventArgs e)
        {
            Global.stop = true;

            PrintLog("정지중....");

            this.button_Stop.Enabled = false;
        }

        private void button_Test_Click(object sender, EventArgs e)
        {
            var dr = listPageCha[0];
            dr.CollectCookie();
            string item = "06무0650";
            bool ret = dr.RegiItemByHttp(item);
            if (ret)
            {
                PrintLog($"{item} 등록 성공");
                var retWait = dr.RegWait(item);
                if (retWait["errCode"].ToString() == "0")
                {
                    PrintLog($"{item} 대기 이동 성공");
                }
                else
                {
                    PrintLog($"{item} 대기 이동 실패");
                }
            }
            else
            {
                PrintLog($"{item} 등록 실패");
            }

            //var top = car.GetListItemByHttp();
        }

        private void comboBox_Kind_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
