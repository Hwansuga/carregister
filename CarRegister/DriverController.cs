﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


public enum TYPE_DRIVER
{
    TYPE_Chrome = 0,
    TYPE_IE = 1,
}

public class DriverController
{
    public ChromeDriver driver;

    public string IsAlertPresent(bool accept_ = true)
    {
        try
        {
            string msg = driver.SwitchTo().Alert().Text;
            if (accept_)
                driver.SwitchTo().Alert().Accept();
            else
                driver.SwitchTo().Alert().Dismiss();
            return msg;
        }
        catch
        {
            return "";
        }

        Thread.Sleep(1000);
    }  
    
    public void GetCookieFromDriver(DriverController otherDrvier_)
    {
        if (driver == null)
            return;

        foreach (var item in otherDrvier_.driver.Manage().Cookies.AllCookies)
            driver.Manage().Cookies.AddCookie(item);
    } 

    public void MoveScroll(IWebElement element_)
    {
        string strPosOrder = $"window.scroll({element_.Location.X},{element_.Location.Y - 100})";
        driver.ExecuteScript(strPosOrder, "");

        //System.Threading.Thread.Sleep(100);
    }

    public IList<IWebElement> FindMyElements(By by)
    {
        WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
        wait.Until(ExpectedConditions.ElementExists(by));
        return driver.FindElements(by);
    }

    public IWebElement FindMyElement(By by)
    {
        WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
        wait.Until(ExpectedConditions.ElementExists(by));
        IWebElement element = driver.FindElement(by);
        MoveScroll(element);
        return element;
    }
    public IWebElement ClickElement(By by, bool moveScrol_ = true)
    {
        IWebElement element = driver.FindElement(by);
        if (moveScrol_)
            MoveScroll(element);
        WaitUntilExist(by);
        element.Click();
        System.Threading.Thread.Sleep(100);
        return element;
    }

    public void ClickElement(IWebElement element_, bool moveScrol_ = true , int sleepTime_ = 500)
    {
        if(moveScrol_)
        {
            MoveScroll(element_);
            Thread.Sleep(sleepTime_);
        }           
        element_.Click();
        System.Threading.Thread.Sleep(sleepTime_);
    }

    public void QuiteDriver()
    {
        if (driver == null)
            return;

        driver.Quit();
        driver = null;
    }

    public void LoadPageComplete()
    {
        new WebDriverWait(driver, TimeSpan.FromSeconds(5)).Until(d => ((IJavaScriptExecutor)d).ExecuteScript("return document.readyState").Equals("complete"));
    }

    public void WaitUntilExist(By by, int sleepTime = 1000)
    {
        try
        {           
            Thread.Sleep(sleepTime);
            LoadPageComplete();
            new WebDriverWait(driver, TimeSpan.FromSeconds(5)).Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(by));
            Thread.Sleep(500);
        }
        catch
        {
            return;
        }
    }

    public void CreatDriver(TYPE_DRIVER type_ = TYPE_DRIVER.TYPE_Chrome, bool showBrowser_ = true , bool disableImg_ = true)
    {
        QuiteDriver();

        switch (type_)
        {
            case TYPE_DRIVER.TYPE_Chrome:
                Create_Chrome(showBrowser_, disableImg_);
                break;
        }
    }

    public bool IsInPage(string msg_)
    {
        if (driver.PageSource.Contains(msg_))
            return true;
        return false;
    }

    public void Create_Chrome(bool showBrowser_ , bool disableImg_ = true)
    {
        
        var driverService = ChromeDriverService.CreateDefaultService();
        driverService.HideCommandPromptWindow = true;

        var options = new ChromeOptions();

        if(showBrowser_ == false)
        {
            options.AddArgument("--headless");
            options.AddUserProfilePreference("profile.default_content_setting_values.images", 2);
        }

//         ChromePerformanceLoggingPreferences logPrefs = new ChromePerformanceLoggingPreferences();
//         options.PerformanceLoggingPreferences = logPrefs;
        options.SetLoggingPreference("performance", LogLevel.All);

        //options.AddArgument("no-sandbox");
        options.AddArgument("--disable-popup-blocking");
        options.AddArgument("--disable-infobars");
        options.AddArgument("--incognito");
        options.AddArgument("--disable-extensions");
        options.AddArgument("--disable-notifications");
        options.AddArgument("--silent");
        options.AddArgument("-js-flags=--expose-gc");
        if (disableImg_)
        {
            options.AddArgument("--blink-settings=imagesEnabled=false");
            options.AddArgument("--disable-images");
        }
        


        //options.AddArgument("--disable-gpu");
        //options.AddArgument("--window-size=1920,1080");
        //options.AddArgument("--lang=ko_KR");
        //options.AddExtension("Block-image_v1.0.crx");
        //options.AddArgument("--blink-settings=imagesEnabled=false");
        //options.AddArgument("--disable-images");

        //string CacheDir = Application.StartupPath + @"\PCUserData\";
        //options.AddArgument(string.Concat("--disk-cache-dir=" + CacheDir + "cache"));


        driver = new ChromeDriver(driverService, options);
        driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(90);
        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

    }

    public void MouseOverAction(By path_)
    {
        IWebElement menu = driver.FindElement(path_);
        Actions builder = new Actions(driver);
        builder.MoveToElement(menu).Build().Perform();
    }

    public void MouseOverAction(IWebElement webElem_)
    {
        Actions builder = new Actions(driver);
        builder.MoveToElement(webElem_).Build().Perform();
    }

    public bool FocusWindow(string keyWord_)
    {
        ReadOnlyCollection<string> tabWindows = driver.WindowHandles;
        foreach (string tabItem in tabWindows)
        {
            if (driver.SwitchTo().Window(tabItem).Title == keyWord_)
                return true;
        }

        return false;
    }

    public bool IsContainAsHtml(IWebElement webElem_,string keyWord_)
    {
        string innerHTML = webElem_.GetAttribute("innerHTML");
        if (innerHTML.Contains(keyWord_))
            return true;
        return false;
    }

    public IWebElement GetParent(IWebElement e)
    {
        return e.FindElement(By.XPath(".."));
    }

    public bool Scroll_PageDown(IWebElement webelement, int scrollPoints)
    {
        try
        {
            Actions dragger = new Actions(driver);
            // drag downwards
            int numberOfPixelsToDragTheScrollbarDown = 10;
            for (int i = 10; i < scrollPoints; i = i + numberOfPixelsToDragTheScrollbarDown)
            {
                dragger.MoveToElement(webelement).ClickAndHold().MoveByOffset(0, numberOfPixelsToDragTheScrollbarDown).Release(webelement).Build().Perform();
            }
            Thread.Sleep(500);
            return true;
        }
        catch
        {
            return false;
        }
    }

    public bool Scroll_PageUp(IWebElement webelement, int scrollPoints)
    {
        try
        {
            Actions dragger = new Actions(driver);
            // drag downwards
            int numberOfPixelsToDragTheScrollbarDown = 10;
            for (int i = 10; i < scrollPoints; i = i + numberOfPixelsToDragTheScrollbarDown)
            {
                dragger.MoveToElement(webelement).ClickAndHold().MoveByOffset(0, -numberOfPixelsToDragTheScrollbarDown).Release(webelement).Build().Perform();
            }
            Thread.Sleep(500);
            return true;
        }
        catch
        {
            return false;
        }
    }
    public void JustMouseHover(string xpath_)
    {

        WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        var element = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(xpath_)));
        Actions action = new Actions(driver);
        action.MoveToElement(element).Perform();
        //Waiting for the menu to be displayed    
        System.Threading.Thread.Sleep(4000);
    }

    public void setAttribute(IWebElement element, string attName, string attValue)
    {
        driver.ExecuteScript("arguments[0].setAttribute(arguments[1], arguments[2]);",
                element, attName, attValue);
    }

}

