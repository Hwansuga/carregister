﻿namespace CarRegister
{
    partial class Form_CarRegister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_CarRegister));
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel_StartTime = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.dateTimePicker_Open = new System.Windows.Forms.DateTimePicker();
            this.button_Reset = new System.Windows.Forms.Button();
            this.button_Stop = new System.Windows.Forms.Button();
            this.button_Start = new System.Windows.Forms.Button();
            this.button_ReadyRegister = new System.Windows.Forms.Button();
            this.button_Ready = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel_Option = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.richTextBox_Log = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label_RemainSec = new System.Windows.Forms.Label();
            this.button_Test = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel_StartTime.SuspendLayout();
            this.panel_Option.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(76)))));
            this.panel1.Controls.Add(this.button_Test);
            this.panel1.Controls.Add(this.tableLayoutPanel_StartTime);
            this.panel1.Controls.Add(this.button_Reset);
            this.panel1.Controls.Add(this.button_Stop);
            this.panel1.Controls.Add(this.button_Start);
            this.panel1.Controls.Add(this.button_ReadyRegister);
            this.panel1.Controls.Add(this.button_Ready);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(166, 450);
            this.panel1.TabIndex = 0;
            // 
            // tableLayoutPanel_StartTime
            // 
            this.tableLayoutPanel_StartTime.ColumnCount = 1;
            this.tableLayoutPanel_StartTime.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_StartTime.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel_StartTime.Controls.Add(this.dateTimePicker_Open, 0, 1);
            this.tableLayoutPanel_StartTime.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel_StartTime.Location = new System.Drawing.Point(0, 397);
            this.tableLayoutPanel_StartTime.Name = "tableLayoutPanel_StartTime";
            this.tableLayoutPanel_StartTime.RowCount = 2;
            this.tableLayoutPanel_StartTime.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_StartTime.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_StartTime.Size = new System.Drawing.Size(166, 53);
            this.tableLayoutPanel_StartTime.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Gainsboro;
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(160, 26);
            this.label3.TabIndex = 9;
            this.label3.Text = "시작시간";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dateTimePicker_Open
            // 
            this.dateTimePicker_Open.CustomFormat = "yyyy/MM/dd HH:mm:ss";
            this.dateTimePicker_Open.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_Open.Location = new System.Drawing.Point(3, 29);
            this.dateTimePicker_Open.Name = "dateTimePicker_Open";
            this.dateTimePicker_Open.Size = new System.Drawing.Size(157, 21);
            this.dateTimePicker_Open.TabIndex = 6;
            // 
            // button_Reset
            // 
            this.button_Reset.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_Reset.FlatAppearance.BorderSize = 0;
            this.button_Reset.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button_Reset.ForeColor = System.Drawing.Color.Gainsboro;
            this.button_Reset.Location = new System.Drawing.Point(0, 157);
            this.button_Reset.Name = "button_Reset";
            this.button_Reset.Size = new System.Drawing.Size(166, 29);
            this.button_Reset.TabIndex = 5;
            this.button_Reset.Text = "리셋";
            this.button_Reset.UseVisualStyleBackColor = true;
            this.button_Reset.Click += new System.EventHandler(this.button_Reset_Click);
            // 
            // button_Stop
            // 
            this.button_Stop.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_Stop.FlatAppearance.BorderSize = 0;
            this.button_Stop.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button_Stop.ForeColor = System.Drawing.Color.Gainsboro;
            this.button_Stop.Location = new System.Drawing.Point(0, 128);
            this.button_Stop.Name = "button_Stop";
            this.button_Stop.Size = new System.Drawing.Size(166, 29);
            this.button_Stop.TabIndex = 4;
            this.button_Stop.Text = "정지";
            this.button_Stop.UseVisualStyleBackColor = true;
            this.button_Stop.Click += new System.EventHandler(this.button_Stop_Click);
            // 
            // button_Start
            // 
            this.button_Start.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_Start.FlatAppearance.BorderSize = 0;
            this.button_Start.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button_Start.ForeColor = System.Drawing.Color.Gainsboro;
            this.button_Start.Location = new System.Drawing.Point(0, 99);
            this.button_Start.Name = "button_Start";
            this.button_Start.Size = new System.Drawing.Size(166, 29);
            this.button_Start.TabIndex = 3;
            this.button_Start.Text = "시작";
            this.button_Start.UseVisualStyleBackColor = true;
            this.button_Start.Click += new System.EventHandler(this.button_Start_Click);
            // 
            // button_ReadyRegister
            // 
            this.button_ReadyRegister.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_ReadyRegister.FlatAppearance.BorderSize = 0;
            this.button_ReadyRegister.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button_ReadyRegister.ForeColor = System.Drawing.Color.Gainsboro;
            this.button_ReadyRegister.Location = new System.Drawing.Point(0, 70);
            this.button_ReadyRegister.Name = "button_ReadyRegister";
            this.button_ReadyRegister.Size = new System.Drawing.Size(166, 29);
            this.button_ReadyRegister.TabIndex = 2;
            this.button_ReadyRegister.Text = "창 생성";
            this.button_ReadyRegister.UseVisualStyleBackColor = true;
            this.button_ReadyRegister.Click += new System.EventHandler(this.button_ReadyRegister_Click);
            // 
            // button_Ready
            // 
            this.button_Ready.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_Ready.FlatAppearance.BorderSize = 0;
            this.button_Ready.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button_Ready.ForeColor = System.Drawing.Color.Gainsboro;
            this.button_Ready.Location = new System.Drawing.Point(0, 41);
            this.button_Ready.Name = "button_Ready";
            this.button_Ready.Size = new System.Drawing.Size(166, 29);
            this.button_Ready.TabIndex = 1;
            this.button_Ready.Text = "준비";
            this.button_Ready.UseVisualStyleBackColor = true;
            this.button_Ready.Click += new System.EventHandler(this.button_Ready_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(39)))), ((int)(((byte)(58)))));
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(166, 41);
            this.panel2.TabIndex = 0;
            // 
            // panel_Option
            // 
            this.panel_Option.Controls.Add(this.panel3);
            this.panel_Option.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_Option.Location = new System.Drawing.Point(166, 0);
            this.panel_Option.Name = "panel_Option";
            this.panel_Option.Size = new System.Drawing.Size(119, 450);
            this.panel_Option.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(39)))), ((int)(((byte)(58)))));
            this.panel3.Controls.Add(this.label1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(119, 41);
            this.panel3.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Gainsboro;
            this.label1.Location = new System.Drawing.Point(37, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "옵션";
            // 
            // richTextBox_Log
            // 
            this.richTextBox_Log.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.richTextBox_Log.Location = new System.Drawing.Point(285, 41);
            this.richTextBox_Log.Name = "richTextBox_Log";
            this.richTextBox_Log.Size = new System.Drawing.Size(464, 409);
            this.richTextBox_Log.TabIndex = 2;
            this.richTextBox_Log.Text = "";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(439, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 11;
            this.label5.Text = "남은시간 : ";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_RemainSec
            // 
            this.label_RemainSec.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_RemainSec.AutoSize = true;
            this.label_RemainSec.ForeColor = System.Drawing.Color.Black;
            this.label_RemainSec.Location = new System.Drawing.Point(535, 13);
            this.label_RemainSec.Name = "label_RemainSec";
            this.label_RemainSec.Size = new System.Drawing.Size(17, 12);
            this.label_RemainSec.TabIndex = 12;
            this.label_RemainSec.Text = "...";
            this.label_RemainSec.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button_Test
            // 
            this.button_Test.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_Test.FlatAppearance.BorderSize = 0;
            this.button_Test.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button_Test.ForeColor = System.Drawing.Color.Gainsboro;
            this.button_Test.Location = new System.Drawing.Point(0, 186);
            this.button_Test.Name = "button_Test";
            this.button_Test.Size = new System.Drawing.Size(166, 29);
            this.button_Test.TabIndex = 10;
            this.button_Test.Text = "Test";
            this.button_Test.UseVisualStyleBackColor = true;
            this.button_Test.Click += new System.EventHandler(this.button_Test_Click);
            // 
            // Form_CarRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(749, 450);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label_RemainSec);
            this.Controls.Add(this.richTextBox_Log);
            this.Controls.Add(this.panel_Option);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form_CarRegister";
            this.Text = "차 등록기";
            this.Load += new System.EventHandler(this.Form_CarRegister_Load);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel_StartTime.ResumeLayout(false);
            this.tableLayoutPanel_StartTime.PerformLayout();
            this.panel_Option.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel_Option;
        private System.Windows.Forms.RichTextBox richTextBox_Log;
        private System.Windows.Forms.Button button_Stop;
        private System.Windows.Forms.Button button_Start;
        private System.Windows.Forms.Button button_ReadyRegister;
        private System.Windows.Forms.Button button_Ready;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_Reset;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_StartTime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dateTimePicker_Open;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label_RemainSec;
        private System.Windows.Forms.Button button_Test;
    }
}