﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarRegister;

public class AccInfo
{
    public string id { get; set; }
    public string pw { get; set; }
}

class Global : Singleton<Global>
{
    public static Dictionary<string, string> dicCodeName = new Dictionary<string, string>()
    {
        {"수원" , "https://dealer.carmodoo.com/car/carListPhoto.html?searchChecker=1&mode=&pageSize=20&no=&m_id=&lastAction=&cho=&maker=&model=&series=&saveFlag=&ord=&ordKey=c_regDate&a_area=%EA%B2%BD%EA%B8%B0&a_gugun=25&c_acNo=533&c_cho=&c_bmNo=&c_boInitNo=&c_boNo=&c_bsNo=&c_bdNo=&c_exCate1=&c_exCate3=&c_year1=&c_month1=&c_year2=&c_month2=&c_mileage1=&c_mileage2=&c_exCate2=&c_price1=&c_price2=&c_carNum=&c_dealerName=&c_sangsaName=&c_dealerHp=&carName=&ordKey=c_regDate&a_area=%B0%E6%B1%E2&a_gugun=25&pageSize=10"},
        {"용인" , "https://dealer.carmodoo.com/car/carListPhoto.html?searchChecker=1&mode=&pageSize=20&no=&m_id=&lastAction=&cho=&maker=&model=&series=&saveFlag=&ord=&ordKey=c_regDate&a_area=%EA%B2%BD%EA%B8%B0&a_gugun=36&c_acNo=491&c_cho=&c_bmNo=&c_boInitNo=&c_boNo=&c_bsNo=&c_bdNo=&c_exCate1=&c_exCate3=&c_year1=&c_month1=&c_year2=&c_month2=&c_mileage1=&c_mileage2=&c_exCate2=&c_price1=&c_price2=&c_carNum=&c_dealerName=&c_sangsaName=&c_dealerHp=&carName=&ordKey=c_regDate&a_area=%B0%E6%B1%E2&a_gugun=36&pageSize=10" }
    };

    public static Dictionary<string, string> dicGubun = new Dictionary<string, string>();
    public static Dictionary<string, string> dicAcNo = new Dictionary<string, string>();

    public static string comboBox_Kind;
    public static bool stop = false;
#if VER2
    public static Form_CarRegister uiController = null;
#else
    public static Form1 uiController = null;
#endif


    public static string topItem = "";
    
    public static AccInfo carmodoo = new AccInfo();

    public static int numericUpDown_CntAcc = 1;
    public static int numericUpDown_CntGoal = 10;
    public static int numericUpDown_Delay = 10;

    public static Dictionary<string, List<string>> dicDB = new Dictionary<string, List<string>>();
    public static string desc = "1";

    public static void LoadAccInfo()
    {
        var data = File.ReadAllText("AccInfo.json");
        carmodoo = Newtonsoft.Json.JsonConvert.DeserializeObject<AccInfo>(data);
    }

    public static string GetUrl()
    {
        return dicCodeName[comboBox_Kind];
    }

    public static void LoadDB()
    {
        var listFiles = Util.GetFileList(@"DB" , "txt");
        foreach(var file in listFiles)
        {          
            var db = File.ReadAllLines(file.FullName);
            dicDB.Add(file.Name , db.ToList());
            uiController.PrintLog($"{file.Name} Load - {db.Length}");
        }
    }

    public static void LoadDesc()
    {
        desc = File.ReadAllText("Desc.txt");
    }

    public static List<string> GetDB(string key_)
    {
        if (dicDB.ContainsKey(key_))
        {
            return dicDB[key_];
        }
        else
        {
            var db = new List<string>();
            dicDB.Add(key_, db);
            return db;
        }
    }

    public static void UpdateDB(string key_)
    {
        if (dicDB.ContainsKey(key_))
        {
            Util.SaveFile($@"DB\{key_}" , dicDB[key_]);
        }
    }

    public static void DeleteDB()
    {
        Directory.Delete(@"DB", true);
    }
}

